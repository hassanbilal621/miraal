/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : restaurant

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-02-29 19:36:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `register_date` timestamp NULL DEFAULT NULL,
  `staff_role` varchar(255) DEFAULT NULL,
  `assigned_class` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('2', 'Hassan Jahangirss', 'admin', '$2y$10$2UOlfn6u8P.Gu7y52sMh3OFtICgFhcd1dNovIOheLu2/IetdylQf6', null, null, null);

-- ----------------------------
-- Table structure for optiongroups
-- ----------------------------
DROP TABLE IF EXISTS `optiongroups`;
CREATE TABLE `optiongroups` (
  `OptionGroupID` int(11) NOT NULL AUTO_INCREMENT,
  `OptionGroupName` varchar(50) COLLATE latin1_german2_ci DEFAULT NULL,
  PRIMARY KEY (`OptionGroupID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

-- ----------------------------
-- Records of optiongroups
-- ----------------------------
INSERT INTO `optiongroups` VALUES ('1', 'color');
INSERT INTO `optiongroups` VALUES ('2', 'size');

-- ----------------------------
-- Table structure for options
-- ----------------------------
DROP TABLE IF EXISTS `options`;
CREATE TABLE `options` (
  `OptionID` int(11) NOT NULL AUTO_INCREMENT,
  `OptionGroupID` int(11) DEFAULT NULL,
  `OptionName` varchar(50) COLLATE latin1_german2_ci DEFAULT NULL,
  PRIMARY KEY (`OptionID`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

-- ----------------------------
-- Records of options
-- ----------------------------
INSERT INTO `options` VALUES ('1', '1', 'red');
INSERT INTO `options` VALUES ('2', '1', 'blue');
INSERT INTO `options` VALUES ('3', '1', 'green');
INSERT INTO `options` VALUES ('4', '2', 'S');
INSERT INTO `options` VALUES ('5', '2', 'M');
INSERT INTO `options` VALUES ('6', '2', 'L');
INSERT INTO `options` VALUES ('7', '2', 'XL');
INSERT INTO `options` VALUES ('8', '2', 'XXL');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `orderid` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) COLLATE latin1_german2_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE latin1_german2_ci DEFAULT NULL,
  `email` varchar(255) COLLATE latin1_german2_ci DEFAULT NULL,
  `address` varchar(255) COLLATE latin1_german2_ci DEFAULT NULL,
  `city` varchar(255) COLLATE latin1_german2_ci DEFAULT NULL,
  `payment` varchar(255) COLLATE latin1_german2_ci DEFAULT NULL,
  `comment` varchar(255) COLLATE latin1_german2_ci DEFAULT NULL,
  `totalamount` varchar(255) COLLATE latin1_german2_ci DEFAULT NULL,
  `status` varchar(255) COLLATE latin1_german2_ci DEFAULT NULL,
  PRIMARY KEY (`orderid`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('10', 'Hamza Ali Khan', '03112369317', 'hamza@gmail.com', 'PAF, BASE Faisal', 'Karachi', 'Cash On Delivery', 'Good', '2200', null);
INSERT INTO `orders` VALUES ('11', 'Hamza Ali Khan', '03112369317', 'hamza@gmail.com', 'PAF, BASE Faisal', 'Karachi', 'Cash On Delivery', 'good', '300', null);
INSERT INTO `orders` VALUES ('12', 'Hamza Ali Khan', '03112369317', 'saifi@gmail.com', 'PAF, BASE Faisal', 'Karachi', 'Cash On Delivery', 'good', '800', null);
INSERT INTO `orders` VALUES ('13', 'Hamza Ali Khan', '03112369317', 'saifi@gmail.com', 'PAF, BASE Faisal', 'Karachi', 'Cash On Delivery', 'good', '800', null);
INSERT INTO `orders` VALUES ('14', 'Hamza Ali Khan', '03112369317', 'saifi@gmail.com', 'PAF, BASE Faisal', 'Karachi', 'Cash On Delivery', 'good', '800', null);

-- ----------------------------
-- Table structure for order_item
-- ----------------------------
DROP TABLE IF EXISTS `order_item`;
CREATE TABLE `order_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `qty` varchar(255) DEFAULT NULL,
  `sub_total` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of order_item
-- ----------------------------
INSERT INTO `order_item` VALUES ('13', '11', '999', '1', '300');

-- ----------------------------
-- Table structure for productcategories
-- ----------------------------
DROP TABLE IF EXISTS `productcategories`;
CREATE TABLE `productcategories` (
  `CategoryID` int(11) NOT NULL AUTO_INCREMENT,
  `CategoryName` varchar(50) COLLATE latin1_german2_ci NOT NULL,
  `Categoryimg` varchar(255) COLLATE latin1_german2_ci DEFAULT NULL,
  PRIMARY KEY (`CategoryID`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

-- ----------------------------
-- Records of productcategories
-- ----------------------------
INSERT INTO `productcategories` VALUES ('19', 'Rice', 'download1.jpg');

-- ----------------------------
-- Table structure for productoptions
-- ----------------------------
DROP TABLE IF EXISTS `productoptions`;
CREATE TABLE `productoptions` (
  `ProductOptionID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ProductID` int(10) unsigned NOT NULL,
  `OptionID` int(10) unsigned NOT NULL,
  `OptionPriceIncrement` double DEFAULT NULL,
  `OptionGroupID` int(11) NOT NULL,
  PRIMARY KEY (`ProductOptionID`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

-- ----------------------------
-- Records of productoptions
-- ----------------------------
INSERT INTO `productoptions` VALUES ('1', '1', '1', '0', '1');
INSERT INTO `productoptions` VALUES ('2', '1', '2', '0', '1');
INSERT INTO `productoptions` VALUES ('3', '1', '3', '0', '1');
INSERT INTO `productoptions` VALUES ('4', '1', '4', '0', '2');
INSERT INTO `productoptions` VALUES ('5', '1', '5', '0', '2');
INSERT INTO `productoptions` VALUES ('6', '1', '6', '0', '2');
INSERT INTO `productoptions` VALUES ('7', '1', '7', '2', '2');
INSERT INTO `productoptions` VALUES ('8', '1', '8', '2', '2');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `ProductID` int(12) NOT NULL AUTO_INCREMENT,
  `ProductSKU` varchar(50) COLLATE latin1_german2_ci NOT NULL,
  `ProductName` varchar(100) COLLATE latin1_german2_ci NOT NULL,
  `ProductPrice` float NOT NULL,
  `ProductWeight` float NOT NULL,
  `ProductCartDesc` varchar(250) COLLATE latin1_german2_ci NOT NULL,
  `ProductShortDesc` varchar(1000) COLLATE latin1_german2_ci NOT NULL,
  `ProductLongDesc` text COLLATE latin1_german2_ci NOT NULL,
  `ProductThumb` varchar(100) COLLATE latin1_german2_ci NOT NULL,
  `ProductCategoryID` int(11) DEFAULT NULL,
  `ProductUpdateDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `ProductStock` float DEFAULT NULL,
  `ProductLive` tinyint(1) DEFAULT 0,
  `ProductUnlimited` tinyint(1) DEFAULT 1,
  `ProductLocation` varchar(250) COLLATE latin1_german2_ci DEFAULT NULL,
  `product_brand_id` int(11) NOT NULL,
  `status` varchar(255) COLLATE latin1_german2_ci DEFAULT NULL,
  `productimg_id` int(255) DEFAULT NULL,
  `seller_id` int(11) DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  PRIMARY KEY (`ProductID`)
) ENGINE=MyISAM AUTO_INCREMENT=1000 DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('999', '0002', 'Biryani', '100', '1', '', 'fefvregfverveav', '', '', '19', '2020-02-29 11:52:32', '0', '0', '0', '', '0', null, null, null, '14:02:00', '03:03:00');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', 'Manager');
INSERT INTO `role` VALUES ('2', 'Accountant');

-- ----------------------------
-- Table structure for staff
-- ----------------------------
DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`staff_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of staff
-- ----------------------------
INSERT INTO `staff` VALUES ('1', 'Hamza Ali Khan', '030393740074', 'hamza@gmail.com', null, '2020-01-16 13:49:50', '1');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(500) COLLATE latin1_german2_ci DEFAULT '',
  `Password` varchar(500) COLLATE latin1_german2_ci DEFAULT '',
  `UserFirstName` varchar(50) COLLATE latin1_german2_ci DEFAULT NULL,
  `UserLastName` varchar(50) COLLATE latin1_german2_ci DEFAULT NULL,
  `username` varchar(50) COLLATE latin1_german2_ci DEFAULT '',
  `UserCity` varchar(90) COLLATE latin1_german2_ci DEFAULT NULL,
  `UserState` varchar(20) COLLATE latin1_german2_ci DEFAULT NULL,
  `UserZip` varchar(12) COLLATE latin1_german2_ci DEFAULT NULL,
  `UserEmailVerified` tinyint(1) DEFAULT 0,
  `UserRegistrationDate` timestamp NULL DEFAULT current_timestamp(),
  `UserVerificationCode` varchar(20) COLLATE latin1_german2_ci DEFAULT NULL,
  `UserIP` varchar(50) COLLATE latin1_german2_ci DEFAULT NULL,
  `UserPhone` varchar(20) COLLATE latin1_german2_ci DEFAULT NULL,
  `UserFax` varchar(20) COLLATE latin1_german2_ci DEFAULT NULL,
  `UserCountry` varchar(20) COLLATE latin1_german2_ci DEFAULT NULL,
  `UserAddress` varchar(100) COLLATE latin1_german2_ci DEFAULT NULL,
  `UserAddress2` varchar(50) COLLATE latin1_german2_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'apna@gmail.com', '$2y$10$MbcjVN.9GHozlOhibiUifuSKG7Gh1Vudb4RbwDS.js3/bsW9PhZAe', null, null, 'apna', null, null, null, '0', '2019-12-16 15:08:49', null, null, '0324', null, null, 'apnakalakar', null);
INSERT INTO `users` VALUES ('2', 'test2@gmail.com', '$2y$10$VOEjhn/p45woiUCG8MlPTO/J1DUEyrPijwXTpvEER7PZIzZrW/84S', null, null, 'apna', null, null, null, '0', '2019-12-16 15:15:48', null, null, '0324', null, null, 'aaaaaaa', null);
INSERT INTO `users` VALUES ('3', 'shehry@gmail.com', '$2y$10$hMrexBvtkpBsKpkZ1QIFZuWXDtJgGm8pHJo9CKQtnNwP6TQweSGh2', null, null, 'shehry', null, null, null, '0', '2019-12-19 14:13:42', null, null, '0324', null, null, 'blablabla', null);
INSERT INTO `users` VALUES ('4', 'hamza@gmail.com', '$2y$10$hpWI.Trkfwq8WsK44smh8OvcUy21ub30I6gc2W0fla50HspEm1iaK', null, null, 'ali_hamza', null, null, null, '0', '2020-01-18 12:43:17', null, null, '03112346731', null, null, 'PAF', null);
INSERT INTO `users` VALUES ('5', 'saifi@gmail.com', '$2y$10$gLQHkgcmk01irCFjMAcgWO5nSRorqakD6VmwhnjnQzyGLNwdz7SI.', null, null, 'hamza_ali', null, null, null, '0', '2020-01-18 12:51:55', null, null, '45435436', null, null, 'paf', null);
INSERT INTO `users` VALUES ('6', 'saifi123@gmail.com', '$2y$10$GdcVNTbGAF9RpRAVe2WcrO9aOXWiawj4J8ghikvzdutSO2UyIPBGC', null, null, 'saifi_hamza', null, null, null, '0', '2020-01-28 16:04:41', null, null, null, null, null, null, null);
INSERT INTO `users` VALUES ('7', 'saifihamza27@gmail.com', '$2y$10$r7vrqcFR4mYoct5kVQHVLueVJ3youVatDxGU4vAkhlxHuYKhxlX.G', null, null, null, null, null, null, '0', '2020-02-11 17:03:59', null, null, null, null, null, 'abc', null);
INSERT INTO `users` VALUES ('8', 'test@gmaisdd.com', '$2y$10$E3JULDmC0x5U6iXYw2qV0Oh0tLMKVOuaQo1GdTqWmL4WTokYF5U2.', null, null, null, null, null, null, '0', '2020-02-11 17:07:45', null, null, null, null, null, 'asdasdasdasd', null);
