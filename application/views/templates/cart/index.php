<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
   .qty .count {
   color: #000;
   display: inline-block;
   vertical-align: top;
   font-size: 25px;
   font-weight: 700;
   line-height: 30px;
   padding: 0 2px
   ;min-width: 35px;
   text-align: center;
   }
   .qty .plus {
   display: inline-block;
   vertical-align: top;
   color: black;
   width: 30px;
   height: 30px;
   font: 30px/1 Arial,sans-serif;
   text-align: center;
   border-radius: 100%;
   }
   .qty .minus {
   display: inline-block;
   vertical-align: top;
   color: black;
   width: 30px;
   height: 30px;
   font: 30px/1 Arial,sans-serif;
   text-align: center;
   border-radius: 100%;
   background-clip: padding-box;
   }
   div {
   text-align: center;
   }
   .minus:hover{
   background-color: #717fe0 !important;
   }
   .plus:hover{
   background-color: #717fe0 !important;
   }
   /*Prevent text selection*/
   span{
   -webkit-user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   }
   input{  
   width: 0px;
   border: 1px black solid;
   height: auto;
   }
   input::-webkit-outer-spin-button,
   input::-webkit-inner-spin-button {
   -webkit-appearance: none;
   margin: 0;
   }
   input:disabled{
   background-color:white;
   }
</style>
<div class="container" style="width:1000px;">
   <div id="main">
      <?php foreach ($this->cart->contents() as $items): ?>
      <div class="row">
         <div class="col s12 m12 l12">
            <div id="basic-tabs check" class="card card card-default scrollspy">
               <div class="card-content pb-5">
                  <div class="col s12 m1 l1"style="padding: 15px 16px 16px 17px;">
                     <P>1.</P>
                  </div>
                  <div class="col s12 m1 l1 "><img src="<?php echo base_url(); ?>assets/app-assets/images/cards/watch-2.png" class="responsive-img" alt=""></div>
                  <div class="col s12 m5 l5">
                     <p>  <?php echo $items['name']; ?> </p>
                     <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>
                     <p>
                        <?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>
                        <strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?> ,
                        <?php endforeach; ?>
                     </p>
                     <?php endif; ?>
                  </div>
                  <div class="col s12 m1 l1">
                     <!-- <p>Price: <?php echo $this->cart->format_number($items['price']); ?> </p> -->
                     <p><strong> Total: </strong> <?php echo $this->cart->format_number($items['subtotal']); ?> </p>
                     <!-- <p>24%off</p> -->
                  </div>
                  <div class="qty col s12 m2 l2">
                     <span class="minus bg-dark">-</span>
                     <input type="number" class="count" name="productqty" value="<?php echo $items['qty']; ?>" style="width: 0px;height: auto;border: 1px black solid;">
                     <span class="plus bg-dark">+</span>
                  </div>
                  <a href="<?php echo base_url(); ?>admin/deleterowcart/<?php echo $items['rowid']; ?>" class=" col s12 m2 l2 btn waves-effect waves-light cyan">delete</a>
               </div>
            </div>
         </div>
      </div>
      <?php endforeach; ?>
      <div class="row">
         <div class="col s12 m12 l12">
            <div id="basic-tabs check" class="card card card-default scrollspy">
               <div class="card-content pb-5">
                  <h6 class="blue-text text-darken-2">Terms & Condition</h6>
                  <p>You know, being a test pilot isn't always the healthiest business
                     in the world<span id="dots">...</span><span id="more" style="display: none;">We predict too
                     much for the next year and yet far too little for the next
                     10..</span><a onclick="myFunction()" id="myBtn">Read more</a>
                  </p>
                  <center>
                     <a class="mb-6 btn waves-effect waves-light gradient-45deg-deep-purple-blue btn modal-trigger" href="<?php echo base_url(); ?>shops/" type="submit" >Continue Shopping</a>	
                     <a class="mb-6 btn waves-effect waves-light gradient-45deg green btn modal-trigger" href="<?php echo base_url(); ?>users/billing" type="submit" >CheckOut</a>
                  </center>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<script>
   $(document).ready(function(){
       $('.count').prop('disabled', true);
    			$(document).on('click','.plus',function(){
   		$('.count').val(parseInt($('.count').val()) + 1 );
     		});
         	$(document).on('click','.minus',function(){
     			$('.count').val(parseInt($('.count').val()) - 1 );
     				if ($('.count').val() == 0) {
   				$('.count').val(1);
   			}
     	    	});
   	});
   
   function myFunction() {
   document.getElementById("Check").disabled = true;
   }
   function myFunction() {
    var dots = document.getElementById("dots");
    var moreText = document.getElementById("more");
    var btnText = document.getElementById("myBtn");
   
    if (dots.style.display === "none") {
      dots.style.display = "inline";
      btnText.innerHTML = "Read more"; 
      moreText.style.display = "none";
    } else {
      dots.style.display = "none";
      btnText.innerHTML = "Read less"; 
      moreText.style.display = "inline";
    }
   }
</script>