<div id="main">
   <div class="row">
   <div class="pt-1 pb-0" id="breadcrumbs-wrapper">
         <!-- Search for small screen-->
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title">Manage Customer</h5>
               </div>
               
            </div>
         </div>
      </div>
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Customer</h4>
                  <div class="row">
                  
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Customer Name</th>
                              <th>Email</th>
                              <th>Phone</th>
                              <th>Referral</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                        
                           <tr>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td>
                                 <a href="" class="btn waves-effect waves-light red" type="submit" name="action">Delete
                                 <i class="material-icons left">delete_forever</i>
                                 </a>
                              </td>
                           </tr>
                         
                           </tfoot>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="modal2" class="modal">
                                    <div class="modal-content">
                                      
                                    </div>
                                 </div>

                                 
   <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>

<script type='text/javascript'>


   function loaduserinfo(deal_id){
      // var userid = this.id;
         $.ajax({
            type: "GET",
            url: "<?php echo base_url();?>admin/ajax_edit_dealmodal/"+deal_id,
            success: function(data){
               $(".modal-content").html(data);
               $('#modal2').modal('open');
            }
         });
   }


</script>