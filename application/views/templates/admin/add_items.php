<!-- <style>
   img
   	{
   		max-width:200px;
   		margin-top: 10px;
           display: content;
   	}
      </style> -->
      <div id="main">
   <div class="row">
   <div class="pt-1 pb-0" id="breadcrumbs-wrapper">
         <!-- Search for small screen-->
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title">Add Items</h5>
               </div>
               <div class="col s12 m6 l6 right-align-md">
                  <ol class="breadcrumbs mb-0">
                     <li><a class="btn green" href="<?php echo base_url();?>admin/manageitems">
                        <i class="material-icons center">chrome_reader_mode</i>
                        </a>
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12">
         <div class="card">
            <div class="card-content">
               <div class="row">
                  <div class="col s12">
                    
                     <?php echo form_open_multipart('admin/add_items');?>
                     <div class="col s12">
                        <!-- Form with placeholder -->
                        <div class="row">
                           <div class="input-field col s12">
                              <label style="margin-left:10px;" for="name2">Item Name </label>
                              <input style="height: 3rem; border: 1px solid #e7a922; border-radius:10px " id="name2" type="text" name="item_name" >
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <label style="margin-left:10px;" for="price2">Item Price</label>
                              <input style="height: 3rem; border: 1px solid #e7a922; border-radius:10px " id="price2" type="text" name="item_price" >
                           </div>
                        </div>
                        <div class="row">
                              <div class="input-field col s12">
                                 <span style="margin-left:10px;" for="img2">Item Image</span>
                                 <input  id="img2" type="file" name="userfile">
                              </div>
                           </div>
                        <div class="row">
                           <div class="col s12">
                              <label style="margin-left:10px;" for="menutype">Select Menu type *</label>
                              <div style="height: 3rem; border: 1px solid #e7a922; border-radius:10px" class="selected-box auto-hight">
                                 <select  class="input-field" name="menu_id" required>
                                    <option disabled selected>Select Menu</option>
                                    <?php foreach ($menus as $menu): ?>
                                    <?php if (empty($menu['name'])) { }
                                       else{        
                                       ?>
                                    <option  value="<?php echo $menu['cat_menu_id']; ?>"><?php echo $menu['name']; ?></option>
                                    <?php }?>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col s12">
                              <label style="margin-left:10px;" for="type">Select Item Type *</label>
                              <div style="height: 3rem; border: 1px solid #e7a922; border-radius:10px" class="selected-box auto-hight">
                                 <select  class="input-field" name="type_id" required>
                                    <option disabled selected>Select type</option>
                                    <?php foreach ($types as $type): ?>
                                    <?php if (empty($type['item_type_name'])) { }
                                       else{        
                                       ?>
                                    <option  value="<?php echo $type['item_type_id']; ?>"><?php echo $type['item_type_name']; ?></option>
                                    <?php }?>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                        
                        <div class="row">
                           <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                              <i class="material-icons right">send</i>
                              </button>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php echo form_close();?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!--                               
   <script>
       function openModal(imageid, imagelink){
   
   
           document.getElementById("modalimage").src = imagelink; 
           document.getElementById("deleteimage").href = "<?php echo base_url(); ?>admin/delete_stock_picture/"+imageid+"?stockid=<?php echo $stock['stock_id']; ?>"; 
   
           $('#modal1').modal('open');
           // alert("sadasd");
           // alert(imageid );
   
       }
   
   </script> -->
<script>
   function previewimg(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
   
              reader.onload = function (e) {
                  $('#view')
                      .attr('src', e.target.result);
              };
   
              reader.readAsDataURL(input.files[0]);
          }
      }
</script>

