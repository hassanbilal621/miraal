<div id="main">
      <div class="row">
         <div class="col s12">
            <div class="container">
               <div id="search-page" class="section">
                  <div class="row">
                     <div class="col s12">
                        <div class="row">
                           <div class="col s12">
                              <div class="card z-depth-1">
                                 <?php echo form_open('admin/billing'); ?>
                                 <div class="card-content">
                                    <div class="row">
                                       <h5>Billing Form</h5>
                                       <div class="col l8 m12" style="width:100%;">
                                          <div class="row video">
                                        
                                             </div>
                                             </div>
                                             <div class="col l4 m4 s12 p-3">
                                                <div class="row">
                                                   <div class="col s12">
                                                      <div class="card border-radius-6">
                                                         <form>
                                                            <div class="row">
                                                               <div class="input-field col s12">
                                                                  <h5 class="ml-4"><span class="btn-floating btn-small gradient-45deg-light-blue-cyan">1</span> Billing Address</h5>
                                                                  <hr class="p-0 mb-10">
                                                               </div>
                                                            </div>
                                                            <div class="row margin">
                                                               <div class="input-field col s12">
                                                                  <input id="username" type="text" name="fullname" required>
                                                                  <label for="username" class="center-align">Full Name</label>
                                                               </div>
                                                            </div>
                                                            <div class="row margin">
                                                               <div class="input-field col s12">
                                                                  <input id="number" type="number" name="phone" required>
                                                                  <label for="number">Phone</label>
                                                               </div>
                                                            </div>
                                                            <div class="row margin">
                                                               <div class="input-field col s12">
                                                                  <input id="email" type="email" name="email" required>
                                                                  <label for="email">Email</label>
                                                               </div>
                                                            </div>
                                                            
                                                            <div class="row margin">
                                                               <div class="input-field col s12">
                                                                  <input id="address" type="text" name="address" required>
                                                                  <label for="address">Address</label>
                                                               </div>
                                                            </div>
                                                            <div class="row margin">
                                                               <div class="input-field col s12">
                                                                  <input id="city" type="text" name="city" required>
                                                                  <label for="city">City</label>
                                                               </div>
                                                            </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col l4 m4 s12">
                                             <div class="row">
                                             <div class="col s12">
                                             <div class="card lighten-4 border-radius-6">
                                             <div class="row">
                                             <div class="input-field col s12">
                                             <h5 class="ml-4"><span class="btn-floating btn-small gradient-45deg-light-blue-cyan">2</span> Shipping Method</h5>
                                             <hr class="p-0 mb-10">
                                             <div class="card-content">
                                             <p class="teal-text lighten-2 truncate">Fixed PKR</p>
                                             </div>
                                             </div>
                                             </div>
                                             </div>
                                             </div>
                                             </div>
                                             <div class="row">
                                             <div class="col s12">
                                             <div class="card lighten-4 border-radius-6">
                                             <div class="row">
                                             <div class="input-field col s12">
                                             <h5 class="ml-4"><span class="btn-floating btn-small gradient-45deg-light-blue-cyan">3</span> Payment Method</h5>
                                             <hr class="p-0 mb-10">
                                             <div class="card-content">
                                             <p>
                                             <label>
                                             <input class="gradient-45deg-light-blue-cyan" name="payment" value="Paypal" type="radio" checked />
                                             <span>Paypal</span>
                                             </label>
                                             </p>
                                             <p>
                                             <label>
                                             <input name="payment" value="Credit Card Easy Pay" type="radio" />
                                             <span>Credit Card Easy Pay</span>
                                             </label>
                                             </p>
                                             <p>
                                             <label>
                                             <input class="" name="payment" value="Cash On Delivery" type="radio" />
                                             <span>Cash On Delivery</span>
                                             </label>
                                             </p>
                                             </div>
                                             </div>
                                             </div>
                                             </div>
                                             </div>
                                             </div>
                                            
                                             </div>
                                             <div class="col l4 m4 s12">
                                             <div class="row">
                                             <div class="col s12">
                                             <div class="card z-depth-0 grey lighten-4 border-radius-6 ">
                                             <div class="row">
                                             <div class="input-field col s12">
                                             <h5 class="ml-4">Order Review</h5>
                                             <hr class="p-0 mb-10">
                                             <div class="card-content">
                                            <span>Sub Total : <?php echo $this->cart->total() ?></span><br />
                                             <span>Shipping : </span><br />
                                             <span>Total Items: <?php echo $this->cart->total_items(); ?></span>
                                             </div>
                                             </div>
                                             </div>
                                             </div>
                                             </div>
                                             </div>
                                             <div class="row">
                                             <div class="col s12">
                                             <form action="">
                                             <div class="row">
                                             <div class="input-field col s12">
                                             <span class="helper-text" data-success="right">Comment</span>
                                             <textarea style="height: 7rem;" name="comment" id="desc1" cols="30" rows="10"></textarea>
                                             </div>
                                             </div>
                                             <div class="row">
                                             <div class="input-field col s12">
                                             <button class="btn waves-effect waves-light border-round cyan col s12" type="submit" name="register">Order Place</button>
                                             </div>
                                             </div>
                                             </form>
                                             </div>
                                             </div>
                                             </div>
                                          </div>
                                          <!-- <div class="col l4 m12 right-content border-radius-6">
                                             <h5 class="mt-0">Materialize Admin</h5>
                                             <p>Material Design Admin Template by PIXINVENT</p>
                                             <img class="responsive-img mt-4 p-3 border-radius-6" src="../../../app-assets/images/gallery/34.png"
                                                alt="">
                                             <p class="mt-2 mb-2">Materialize is a Material Design Admin Template is the excellent responsive
                                                google material design inspired multipurpose admin template.
                                             </p>
                                             <hr>
                                             <p class="mt-2"><b class="blue-grey-text text-darken-4">Files Included:</b> HTML Files, CSS Files</p>
                                             <p class="mt-2"><b class="blue-grey-text text-darken-4">Layout:</b> Responsive</p>
                                             <p class="mt-2"><b class="blue-grey-text text-darken-4">Created:</b> 20 May 15</p>
                                             <p class="mt-2">Materialize - Material Design Admin Template by PIXINVENT</p>
                                             <p class="mt-5"><a href="#">Themeforest - $24</a></p>
                                             </div> -->
                                       </div>
                                    </div>
                                 </div>
                                 <?php echo form_close(); ?>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  
                  <!-- END RIGHT SIDEBAR NAV -->
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="modal2" class="modal" style="width:40%;">
   <div class="modal-content" style="width:40%; margin-left:30%;">
      <form method="post" action="do_login.php" onsubmit="return do_login();">
         <div class="login-form">
            <div class="row">
               <div class="input-field col s12">
                  <center><img src="<?php echo base_url(); ?>assets/app-assets/images\logo/kalakaar logo.png" alt="" style="height: 150px; margin: auto;"></center>
               </div>
            </div>
            <div class="row margin">
               <div class="input-field col s12">
                  <i class="material-icons prefix pt-2">person_outline</i>
                  <input id="emailid" type="email" name="username">
                  <label for="username" class="center-align">Email</label>
               </div>
            </div>
            <div class="row margin">
               <div class="input-field col s12">
                  <i class="material-icons prefix pt-2">lock_outline</i>
                  <input id="pass" type="password" name="password">
                  <label for="password">Password</label>
               </div>
            </div>
            <div class="row">
               <div class="col s12 m12 l12 ml-2 mt-1">
                  <p>
                     <label>
                     <input type="checkbox" />
                     <span>Remember Me</span>
                     </label>
                  </p>
               </div>
            </div>
            <div class="row">
               <div class="input-field col s12">
                  <button type="submit" class="btn waves-effect waves-light border-round cyan col s12" >Login</button>
               </div>
            </div>
            <div class="row">
               <div class="input-field col s6 m6 l6">
                  <p class="margin medium-small"><a href="regist" target="_blank">Register Now!</a></p>
               </div>
               <div class="input-field col s6 m6 l6">
                  <p class="margin right-align medium-small"><a href="user-forgot-password.html" target="_blank">Forgot password ?</a></p>
               </div>
            </div>
         </div>
      </form>
   </div>
<script src="<?php echo base_url(); ?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<!-- <script type='text/javascript'>
   function loaduserinfo(loginid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>users/ajax_edit_billing_login/" + loginid,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal2').modal('open');
         }
      });
   }
   </script> -->
<script>
   function do_login() {
      var email = $("#emailid").val();
      var pass = $("#pass").val();
   
      // alert(email);
      // alert(pass);
      if (email != "" && pass != "") {
         $.ajax({
            type: 'get',
            url: '<?php echo base_url(); ?>users/ajaxlogin',
            data: {
               do_login: "do_login",
               email: email,
               password: pass
            },
            success: function(response) {
               if (response == "success") {
                  alert("Login Successfull");
                  $("#setemail").val(email);
                  $('#loginform1').css('display','none');
   
                  $("#loginform1").html("");
   
               } else {
                  alert("Wrong Details");
               }
            }
         });
      } else {
         alert("Please Fill All The Details");
      }
   
      return false;
   }
</script>
<!-- BEGIN VENDOR JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->