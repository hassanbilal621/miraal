<div id="main">
   <div class="row">
      <div class="pt-1 pb-0" id="breadcrumbs-wrapper">
         <!-- Search for small screen-->
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title">Add Menu</h5>
               </div>
               <div class="col s12 m6 l6 right-align-md">
                  <ol class="breadcrumbs mb-0">
                     <li><a class="btn green" href="<?php echo base_url();?>admin/manage_item_type">
                        <i class="material-icons center">chrome_reader_mode</i>
                        </a>
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </div>
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <div class="row">
                     <div class="col s12">
                        <?php echo form_open('admin/item_type');?>
                        <div class="col s12">
                           <!-- Form with placeholder -->
                           <div class="row">
                              <div class="input-field col s12">
                                 <input style="height: 3rem; border: 1px solid #e7a922; border-radius:10px " id="name2" type="text" name="item_type_name">
                                 <label style="margin-left:10px;" for="name2">Name </label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="input-field col s12">
                                 <button style="background-color:#560001;" class="btn waves-effect waves-light right" type="submit" name="action">Submit
                                 <i class="material-icons right">send</i>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <?php echo form_close();?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>
<!-- BEGIN VENDOR JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<div id="modal2" class="modal">
   <div class="modal-content">
   </div>
</div>
<script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<script type='text/javascript'>
   function loaduserinfo(catid){
      // var userid = this.id;
         $.ajax({
            type: "GET",
            url: "<?php echo base_url();?>admin/ajax_edit_categorymodal/"+catid,
            success: function(data){
               $(".modal-content").html(data);
               $('#modal2').modal('open');
            }
         });
   }
   
   
</script>