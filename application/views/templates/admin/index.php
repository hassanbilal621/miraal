    <!-- BEGIN: Page Main-->
    <div id="main">
    <div class="row">
        <div class="pt-3 pb-1" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
			<div class="container">
				<div class="row">
					<div class="col s12 m6 l6">
						<h5 class="breadcrumbs-title mt-0 mb-0">Dashboard</h5>
					</div>
					
				</div>
			</div>
        </div>
        <div class="col s12">
			<div id="card-with-analytics" class="section">
				<div class="row">
					<div class="col s12 m6 l3 card-width">
							<div class="card border-radius-6">
								<div class="card-content right-align">
								<img class="left" style="vertical-align: middle; height: 100px; width: 50px; height:50px;" src="<?php echo base_url(); ?>assets/app-assets/images/icon/menu.svg" alt="avatar">
										<h4 class="m-0"><b>14</b></h4>
										<p>Menu</p>
									
								</div>
							</div>
					</div>
					<div class="col s12 m6 l3 card-width">
							<div class="card border-radius-6">
							<div class="card-content right-align">
								<img class="left" style="vertical-align: middle; height: 100px; width: 50px; height:50px;" src="<?php echo base_url(); ?>assets/app-assets/images/icon/items.svg" alt="avatar">
										<h4 class="m-0"><b>75</b></h4>
										<p>Items</p>
									
								</div>
							</div>
					</div>
					<div class="col s12 m6 l3 card-width">
							<div class="card border-radius-6">
							<div class="card-content right-align">
								<img class="left" style="vertical-align: middle; height: 100px; width: 50px; height:50px;" src="<?php echo base_url(); ?>assets/app-assets/images/icon/addons.svg" alt="avatar">
										<h4 class="m-0"><b>13</b></h4>
										<p>Add-ons</p>
									
								</div>
							</div>
					</div>
					<div class="col s12 m6 l3 card-width">
							<div class="card border-radius-6">
							<div class="card-content right-align">
								<img class="left" style="vertical-align: middle; height: 100px; width: 50px; height:50px;" src="<?php echo base_url(); ?>assets/app-assets/images/icon/options.svg" alt="avatar">
										<h4 class="m-0"><b>15</b></h4>
										<p>Options</p>
									
								</div>
							</div>
					</div>
				</div>

				<div class="row">
					<div class="col s12 m6 l3 card-width">
							<div class="card border-radius-6">
							<div class="card-content right-align">
								<img class="left" style="vertical-align: middle; height: 100px; width: 50px; height:50px;" src="<?php echo base_url(); ?>assets/app-assets/images/icon/offers.svg" alt="avatar">
										<h4 class="m-0"><b>20</b></h4>
										<p>Offers</p>
									
								</div>
							</div>
					</div>
					<div class="col s12 m6 l3 card-width">
							<div class="card border-radius-6">
							<div class="card-content right-align">
								<img class="left" style="vertical-align: middle; height: 100px; width: 50px; height:50px;" src="<?php echo base_url(); ?>assets/app-assets/images/icon/customers.svg" alt="avatar">
										<h4 class="m-0"><b>18</b></h4>
										<p>Customers</p>
									
								</div>
							</div>
					</div>
					<div class="col s12 m6 l3 card-width">
							<div class="card border-radius-6">
							<div class="card-content right-align">
								<img class="left" style="vertical-align: middle; height: 100px; width: 50px; height:50px;" src="<?php echo base_url(); ?>assets/app-assets/images/icon/neworders.svg" alt="avatar">
										<h4 class="m-0"><b>13</b></h4>
										<p>New Orders</p>
									
								</div>
							</div>
					</div>
					</div>
			</div>
        </div>
        <div class="col s12">
          <div class="container">
            <div class="seaction">
        <div id="chartjs-bar-chart" class="card">
      <div class="card-content">
         <h4 class="card-title">Sales Chart</h4>
         <div class="row">
            <div class="col s12">
               <p class="mb-2">
                  A bar chart is a way of showing data as bars. It is sometimes used to show trend data, and the
                  comparison
                  of multiple data sets side by side.
               </p>
               <div class="sample-chart-wrapper"><canvas id="bar-chart" height="400"></canvas></div>
            </div>
         </div>
      </div>
        </div>
            </div>
          </div>
   </div>
    </div>
    </div>
    <!-- END: Page Main-->