<div id="main">
   <div class="row">
   <div class="pt-1 pb-0" id="breadcrumbs-wrapper">
         <!-- Search for small screen-->
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title">Manage Items</h5>
               </div>
               <div class="col s12 m6 l6 right-align-md">
                  <ol class="breadcrumbs mb-0">
                     <li><a class="btn green" href="<?php echo base_url();?>admin/add_items">
                        <i class="material-icons center">add</i>
                        </a>
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </div>
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <div class="row">
                  
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Item Names</th>
                              <th>Item Price</th>
                              <th>Item Type</th>
                              <th>Item Menu</th>
                              <th>Item Image</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($items as $item): ?>
                           <tr>
                              <td><?php echo $item['item_id']; ?></td>
                              <td><?php echo $item['item_name']; ?></td>
                              <td>$<?php echo $item['item_price']; ?></td>
                              <td><?php echo $item['item_type_name']; ?></td>
                              <td><?php echo $item['name']; ?></td>
                              <td><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $item['item_image']; ?>" alt="avatar" style="width:100px; height:100px;"></td>
                              <td>
                                 <button id="<?php echo $item['item_id']; ?>"  onclick="loaduserinfo(this.id)" class="btn waves-effect waves-light blue btn">Edit
                                 <i class="material-icons left">edit</i>
                                 </button>
                                 
                                 <a href="<?php echo base_url(); ?>admin/del_items/<?php echo $item['item_id']; ?>" class="btn waves-effect waves-light red" type="submit" name="action">Delete
                                 <i class="material-icons left">delete_forever</i>
                                 </a>
                              </td>
                           </tr>
                         
                           <?php endforeach; ?>
                           </tfoot>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="modal2" class="modal">
                                    <div class="modal-content">
                                      
                                    </div>
                                 </div>

                                 
   <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>

<script type='text/javascript'>


   function loaduserinfo(item_id){
      // var userid = this.id;
         $.ajax({
            type: "GET",
            url: "<?php echo base_url();?>admin/ajax_edit_itemsmodal/"+item_id,
            success: function(data){
               $(".modal-content").html(data);
               $('#modal2').modal('open');
            }
         });
   }


</script>