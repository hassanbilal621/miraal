<div id="main">
   <div class="row">
   <div class="pt-1 pb-0" id="breadcrumbs-wrapper">
         <!-- Search for small screen-->
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title">Add Offers</h5>
               </div>
               <div class="col s12 m6 l6 right-align-md">
                  <ol class="breadcrumbs mb-0">
                     <li><a class="btn green" href="<?php echo base_url();?>admin/managedeal">
                        <i class="material-icons center">chrome_reader_mode</i>
                        </a>
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </div>
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <div class="row">
                     <div class="col s12">
                        <?php echo form_open_multipart('admin/add_deal');?>
                        <div class="col s12">
                           <!-- Form with placeholder -->
                           <h4 class="card-title">Menu Categories</h4>
                           <div class="row">
                              <div class="input-field col s12">
                                 <input style="height: 3rem; border: 1px solid #e7a922; border-radius:10px " id="name2" type="text" name="deal_name">
                                 <label style="margin-left:10px;" for="name2">Name</label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="input-field col s12">
                                 <input style="height: 3rem; border: 1px solid #e7a922; border-radius:10px " id="price2" type="text" name="deal_price">
                                 <label style="margin-left:10px;" for="price2">Price</label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="input-field col s12">
                                 <input style="height: 3rem; border: 1px solid #e7a922; border-radius:10px " id="desc2" type="text" name="deal_description">
                                 <label style="margin-left:10px;" for="desc2">Description</label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="input-field col s12">
                                 <span style="margin-left:10px;" for="img2">Category Image</span>
                                 <input  id="img2" type="file" name="userfile">
                              </div>
                           </div>
                           <div class="row">
                              <div class="input-field col s12">
                                 <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                 <i class="material-icons right">send</i>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <?php echo form_close();?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
