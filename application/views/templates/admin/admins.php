
    <style>
        table.table tr th, table.table tr td {
            border-color: #e9e9e9;
        }   

    </style>

    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <h4 class="header2">Add Administrators</h4>
                <?php if($this->session->flashdata('admin_added')): ?>                                    
                    <div class="row">
                        <div id="card-alert" class="card gradient-45deg-green-teal">
                            <div class="card-content white-text">
                            <p>
                                <i class="material-icons">check</i> <?php echo $this->session->flashdata('admin_added'); ?> </p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                <?php endif; ?>  
                <?php if($this->session->flashdata('admin_empty')): ?>                                    
                    <div class="row">
                        <div id="card-alert" class="card gradient-45deg-amber-amber">
                            <div class="card-content white-text">
                            <p>
                                <i class="material-icons">check</i> <?php echo $this->session->flashdata('admin_empty'); ?> </p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                <?php endif; ?>  
                <?php if($this->session->flashdata('admin_updated')): ?>                                    
                    <div class="row">
                        <div id="card-alert" class="card gradient-45deg-green-teal">
                            <div class="card-content white-text">
                            <p>
                                <i class="material-icons">check</i> <?php echo $this->session->flashdata('admin_updated'); ?> </p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                <?php endif; ?> 
                <?php if($this->session->flashdata('admin_deleted')): ?>                                    
                    <div class="row">
                        <div id="card-alert" class="card gradient-45deg-green-teal">
                            <div class="card-content white-text">
                            <p>
                                <i class="material-icons">check</i> <?php echo $this->session->flashdata('admin_deleted'); ?> </p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                <?php endif; ?>   
                <?php echo form_open_multipart('admin/addadmin'); ?>
                <div class="row">
                    <div class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="first_name" value="<?php echo set_value('name');?>" name="name" type="text" >
                                <label for="first_name" class="">First Name  <?php echo form_error('name','<span class="error">', '</span>'); ?></label>
                            </div>
                            <div class="input-field col s6">
                                <input id="first_name"  value="<?php echo set_value('username');?>" name="username" type="text">
                                <label for="first_name" class="">User Name  <?php echo form_error('username','<span class="error">', '</span>'); ?></label>
                            </div>
                            <div class="row">
                            <div class="input-field col s12">
                                <input id="password6"  value="<?php echo set_value('password');?>" name="password" type="password">
                                <label for="password">Password  <?php echo form_error('password','<span class="error">', '</span>'); ?></label>
                            </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <div class="row">
                    <div class="container">
                         <div class="table-wrapper">
                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-8"><h5>Administrators</h5></div>
                                </div>  
                            </div>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Username</th>
                                        <th>Registration Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
         
                                <tbody>
                                    <?php foreach ($admins as $admin): ?>
                                    <tr>
                                        <td><?php echo $admin['id']; ?></td>
                                        <td><?php echo $admin['name']; ?></td>
                                        <td><?php echo $admin['username']; ?></td>
                                        <td><?php echo $admin['register_date']; ?></td>
                                        <td>
                                          
                                            <button id="<?php echo $admin['id']; ?>" class="userinfo btn modal-trigger" class="edit" title="Edit" data-toggle="modal" data-target="#myModal" data-myvalue="trouducul" data-myvar="bisbis"><i class="material-icons">&#xE254;</i></button>
                                            <a href="<?php echo base_url(); ?>admin/deladmin/<?php echo $admin['id']; ?>" class="delete" title="Delete" data-toggle="tooltip"> <button class="btn modal-trigger"><i class="material-icons">&#xE872;</i></button></a>
                                        </td>
                                    </tr>
                                    
                                    <?php endforeach; ?>
           
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    

    <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content modal-body">
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>

    <script type='text/javascript'>
        $(document).ready(function(){
            $('.userinfo').click(function(){
                var adminid = this.id;
             
                $.ajax({
                    type: "GET",
                    url: "<?php echo base_url();?>admin/ajax_edit_adminmodal/"+adminid,
                    data:'country_name=pakistan',
                    success: function(data){
                        $(".modal-content").html(data);
                        $('#modal1').modal('open');
                    }
                });
            });
        });
    </script>