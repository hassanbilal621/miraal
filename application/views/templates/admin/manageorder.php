<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Products</h4>
                  <div class="row">
                 
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Order Name</th>
                              <th>Name</th>
                              <th>Price</th>
                              <th>Quantity</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                        <?php foreach($orders as $order):?>
                           <tr>
                              <td><?php echo $order['id'];?></td>
                              <td><?php echo $order['item_name'];?></td>
                              <td><?php echo $order['fullname'];?></td>
                              <td>$<?php echo $order['sub_total'];?></td>
                              <td><?php echo $order['qty'];?></td>
                              <td>
                                 <!-- <a href="<?php echo base_url(); ?>admin/edit_product/<?php echo $product['ProductID'];?>" class="btn waves-effect waves-light blue btn">Edit
                                 <i class="material-icons left">edit</i>
                                 </a> -->
                                 
                                 <a href="<?php echo base_url(); ?>admin/del_placeorder/<?php echo $order['id']; ?>" class="btn waves-effect waves-light red" type="submit" name="action">Delete
                                 <i class="material-icons left">delete_forever</i>
                                 </a>
                              </td>
                           </tr>
                           <?php endforeach;?>
                           
                           </tfoot>
                     </table>
                 
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>