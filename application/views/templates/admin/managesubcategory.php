<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Sub Category</h4>
                  <div class="row">
                     <div class="col s12">
                        
                        
                     </div>
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Sub Category</th>
                              <th>Category</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($subcategories as $subcategory): ?>
                           <tr>
                              <td><?php echo $subcategory['subcat_id']; ?></td>
                              <td><?php echo $subcategory['subcate_name']; ?></td>
                              <td><?php echo $subcategory['CategoryName']; ?></td>
                              <td>
                                 <button id="<?php echo $subcategory['subcat_id']; ?>"  onclick="loaduserinfo(this.id)" class="btn waves-effect waves-light blue btn">Edit
                                 <i class="material-icons left">edit</i>
                                 </button>
                               
                                 <a href="<?php echo base_url(); ?>admin/del_subcat/<?php echo $subcategory['subcat_id']; ?>" class="btn waves-effect waves-light red" type="submit" name="action">Delete
                                 <i class="material-icons left">delete_forever</i>
                                 </a>
                              </td>
                           </tr>
                           <?php endforeach; ?>
                         
                           </tfoot>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>
<!-- BEGIN VENDOR JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->


<div id="modal2" class="modal">
                                    <div class="modal-content">
                                      
                                    </div>
                                 </div>

                                 <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>

<script type='text/javascript'>


   function loaduserinfo(subcatid){
      // var userid = this.id;
         $.ajax({
            type: "GET",
            url: "<?php echo base_url();?>admin/ajax_edit_subcategorymodal/"+subcatid,
            success: function(data){
               $(".modal-content").html(data);
               $('#modal2').modal('open');
            }
         });
   }


</script>