<div id="main">
   <div class="row">
   <div class="pt-1 pb-0" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s12 m6 l6">
                <h5 class="breadcrumbs-title">Add Role</h5>
              </div>
              <div class="col s12 m6 l6 right-align-md">
                <ol class="breadcrumbs mb-0">
                
                  <li class="breadcrumb-item active"><button style="background-color:#560001;" class="waves-effect waves-light btn modal-trigger mb-2 mr-1" href="#modal1">Add Role
                        <i class="material-icons right">shopping_basket</i>
                        </button>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  
                  <div class="row">
                     <div class="col s12">
                        
                        <div id="modal1" class="modal">
                           <div class="modal-content">
                              <div class="row">
                                 <div class="col s12">
                                    <div class="card">
                                    <?php echo form_open('admin/role');?>
                                       <div class="col s12">
                                          <!-- Form with placeholder -->
                                          <h4 class="card-title">Create Role</h4>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input style="height: 3rem; border: 1px solid #e7a922; border-radius:10px " id="name2" type="text" name="role">
                                                <label style="margin-left:10px;" for="name2">Role Name </label>
                                             </div>
                                          </div>
                                          
                                          
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <button class="btn waves-effect waves-light right" type="submit" name="action">Submit
                                                <i class="material-icons right">send</i>
                                                </button>
                                             </div>
                                          </div>
                                    </div>
                                    </div>
                                    <?php echo form_close();?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Role Name</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($roles as $role):?>
                           <tr>
                              <td><?php echo $role['role_id']?></td>
                              <td><?php echo $role['role']?></td>
                             
                              <td>
                                 <button id="<?php echo $role['role_id'];?>" onclick="loaduserinfo(this.id)" class="btn waves-effect waves-light blue btn" >Edit
                                 <i class="material-icons left">edit</i>
                                 </button>
                                
                                 <a href="<?php echo base_url(); ?>admin/del_role/<?php echo $role['role_id']; ?>" class="btn waves-effect waves-light red" type="submit" name="action">Delete
                                 <i class="material-icons left">delete_forever</i>
                                 </a>
                              </td>
                           </tr>
                        <?php endforeach;?>
                           </tfoot>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<div id="modal2" class="modal">
                                    <div class="modal-content">
                                       
                                    </div>
                                 </div>

                                 <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>

<script type='text/javascript'>


   function loaduserinfo(roleid){
      // var userid = this.id;
         $.ajax({
            type: "GET",
            url: "<?php echo base_url();?>admin/ajax_edit_rolemodal/"+roleid,
            success: function(data){
               $(".modal-content").html(data);
               $('#modal2').modal('open');
            }
         });
   }


</script>

<!-- BEGIN VENDOR JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->