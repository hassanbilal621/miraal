<div id="main">
      <div class="row">
      
        <div class="col s12">
          <div class="container">
          
<div class="section" id="user-profile">
  <div class="row">
    <!-- User Profile Feed -->
    <div class="col s12 m8 26 user-section-negative-margin">

<!-- start -->

<div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content">
          <h4 class="card-title">Generate Orders</h4>
          <div class="row">
            <div class="col s12">
            <table id="page-length-option" class="display">
                <thead>
                  <tr>
                    <th>Image</th>
                    <th>Product Name</th>
                    <th>Category</th>
                    <th>Menu</th>
                    <th>Price</th>
                    <th>Select Order</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($items as $item):?>
                  <tr>
                    <td><img src="<?php echo base_url(); ?>assets/app-assets/images/cards/cameras.png" class="responsive-img" style="width:100px; height:100px;" alt=""></td>
                    <td><?php echo $item['item_name']; ?></td>
                    <td><?php echo $item['item_type_name']; ?></td>
                    <td><?php echo $item['name']; ?></td>
                    <td>$<?php echo $item['item_price']; ?></td>
                    <td><a id="<?php echo $item['item_id']; ?>" style="width:100%;" class="col s12 m12 l4 mt-2 waves-effect waves-light gradient-45deg-deep-purple-blue btn modal-trigger"
                                    onclick="ajax_view_product(this.id)">View</a></td>
                  </tr>
                  <?php endforeach;?>
                </tbody>
                <tfoot>
                  <tr>
                  <th>Image</th>
                    <th>Product Name</th>
                    <th>Category</th>
                    <th>Qty</th>
                    <th>Price</th>
                    <th>Select Order</th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!-- End -->

   
    </div>

    <!-- Today Highlight -->
    <div class="col s12 m12 l3 hide-on-med-and-down" style="width:33%;">
   
    
      <div class="row">
        <div class="col s12">
        <div class="card">
        <div class="card-content">
          <h6 class="card-title">Place Orders</h6>
          <table>
    <thead>
    <tr>
      <th class="center">Product</th>
      <th class="center">Quantity</th>
      <th class="center">Price</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($this->cart->contents() as $items): ?>
    <tr>
      <td><?php echo $items['name']; ?></td>
      <p class="latest-update"><?php if ($this->cart->has_options($items['rowid']) == TRUE): ?><span class="right"> <a href="#">+12</a> </span></p>
          <p class="latest-update"><?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>
          <strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?> ,
          <?php endforeach; ?><span class="right"> <a href="#">+570</a> </span></p>
          <?php endif; ?>
      <td><p class="center"><input style="width:20%;" class="" type="number" value="1" name="productqty" required/>
      <a class="mb-2 btn-floating waves-effect waves-light cyan">
                      <i class="material-icons">update</i>
                    </a>
                    <a href="<?php echo base_url(); ?>admin/deleterowcart/<?php echo $items['rowid']; ?>" class="mb-2 btn-floating waves-effect waves-light red accent-2">
                      <i class="material-icons">clear</i>
                    </a>
                  </p></td>
      <td>$<?php echo $this->cart->format_number($items['subtotal']); ?></td>
    </tr>
    <?php endforeach; ?>
    </tbody>
  </table>
  <div class="row">
  <div class="input-field col s12">
  <p class="latest-update">Total Items<span class="right"> <?php echo $this->cart->total_items(); ?> </span></p>
          <p class="latest-update">Grand Total <span class="right"> $<?php echo $this->cart->total() ?> </span></p>
          </div>
          </div>
          <div class="row">
                                             <div class="input-field col s12">
                                             <a href="<?php echo base_url(); ?>admin/billing" class="btn waves-effect waves-light border-round cyan col s12" type="submit" name="register">Order Place</a>
                                             </div>
                                             </div>


          <!-- <p class="latest-update"><?php echo $items['name']; ?><span class="right"> <a href="#">+480</a> </span></p>
          <p class="latest-update"><?php if ($this->cart->has_options($items['rowid']) == TRUE): ?><span class="right"> <a href="#">+12</a> </span></p>
          <p class="latest-update"><?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>
          <strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?> ,
          <?php endforeach; ?><span class="right"> <a href="#">+570</a> </span></p>
          <?php endif; ?>
          <p class="latest-update"><?php echo $this->cart->format_number($items['subtotal']); ?> <span class="right"><a href="#">+120</a> </span></p>
          <p class="latest-update">Qty : <input style="width:10%;" class="" type="number" value="1" name="productqty" required/></p>
          <p><a href="<?php echo base_url(); ?>admin/deleterowcart/<?php echo $items['rowid']; ?>" class=" col s12 m2 l2 btn waves-effect waves-light cyan">delete</a></p>
          <hr> -->
          
         

          
          </div>
          </div>   
        </div>
      </div>
    </div>
  </div>
</div><!-- START RIGHT SIDEBAR NAV -->

          </div>
        </div>
      </div>
    </div>
    <div id="modal1" class="modal">
   <div class="modal-content pt-2 modal-content2">
   </div>
</div>
<script type='text/javascript'>
   function ajax_view_product(item_id){
      // var userid = this.id;
         $.ajax({
            type: "GET",
            url: "<?php echo base_url();?>admin/ajax_view_product/"+item_id,
            data:'country_name=pakistan',
            success: function(data){
               $(".modal-content2").html(data);
               $('#modal1').modal('open');
            }
         });
   }
   </script>