<div id="main">
   <div class="row">
   <div class="pt-1 pb-0" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s12 m6 l6">
                <h5 class="breadcrumbs-title">Add Users</h5>
              </div>
              <div class="col s12 m6 l6 right-align-md">
                <ol class="breadcrumbs mb-0">
                <li><a class="btn green" href="<?php echo base_url();?>admin/add_user">
                        <i class="material-icons center">add</i>
                        </a>
                     </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  
                  <div class="row">
                     <div class="col s12">
                        
                        <div id="modal1" class="modal">
                           <div class="modal-content">
                              <div class="row">
                                 <div class="col s12">
                                    <div class="card">
                                    <?php echo form_open_multipart('admin/client');?>
                                       <div class="col s12">
                                          <!-- Form with placeholder -->
                                          <h4 class="card-title">Create Client</h4>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input id="name2" type="text" name="email">
                                                <label for="name2">Client Email </label>
                                             </div>
                                          </div>
                                          <div class="row">
                                          <div class="input-field col s12">
                                                <input id="username2" type="text" name="username">
                                                <label for="username2">Client Email </label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input id="password2" type="password" name="password">
                                                <label for="password2">Password</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                                <i class="material-icons right">send</i>
                                                </button>
                                             </div>
                                          </div>
                                    </div>
                                    </div>
                                    <?php echo form_close();?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Client Email</th>
                              <th>Client Username</th>
                              <th>Registration Date/Time</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                        <?php foreach($users as $user):?>
                           <tr>
                              <td><?php echo $user['id'];?></td>
                              <td><?php echo $user['email'];?></td>
                              <td><?php echo $user['username'];?></td>
                              <td><?php echo $user['UserRegistrationDate'];?></td>
                              </td>
                              <td>
                                 <button id="<?php echo $user['id'];?>" onclick="loaduserinfo(this.id)" class="btn waves-effect waves-light blue modal-trigger mb-2 mr-1" type="submit"  href="#modal2"  name="action">Edit
                                 <i class="material-icons left">edit</i>
                                 </button>
                                
                                 <button class="btn waves-effect waves-light red" type="submit" name="action">Delete
                                 <i class="material-icons left">delete_forever</i>
                                 </button>
                              </td>
                           </tr>
                           <?php endforeach;?>
                           </tfoot>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="modal2" class="modal">
                                    <div class="modal-content">
                                      
                                    </div>
                                 </div>
                                 <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>

<script type='text/javascript'>


   function loaduserinfo(userid){
      // var userid = this.id;
         $.ajax({
            type: "GET",
            url: "<?php echo base_url();?>admin/ajax_edit_usermodal/"+userid,
            success: function(data){
               $(".modal-content").html(data);
               $('#modal2').modal('open');
            }
         });
   }


</script>


<!-- BEGIN VENDOR JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->