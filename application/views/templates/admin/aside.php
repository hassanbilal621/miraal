<!-- BEGIN: SideNav-->
<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">
   <div class="brand-sidebar" >
      <a href="<?php echo base_url(); ?>admin/" class="darken-1" >
      <span class="logo-text hide-on-med-and-down" style=" font-size:46px; visibility: visible; padding-left: 30px;"> <img src="<?php echo base_url(); ?>assets/app-assets/images/logo/Miraal.png" style="width:150px !important; height:50px !important; margin-top: 6px;" alt="Miraal"></span>
      </a>
      <!-- <a class="brand-logo darken-1" ><img src="https://i.imgur.com/zYMsdbC.png" alt=""/> <span class="logo-text hide-on-med-and-down">Materialize</span> </a> -->
      <!-- <h1 class="logo-wrapper"><a class="darken-1" href="index.html"><img src="https://i.imgur.com/zYMsdbC.png" alt=""/><span class="logo-text hide-on-med-and-down">Materialize</span></a><a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a></h1> -->
   </div>
   <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
      <li  class="navigation-header"><a class="navigation-header-text">Dashboard</a><i class="navigation-header-icon material-icons">more_horiz</i>
      </li>
      <li  class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/"><i style="color:#e7a922;" class="material-icons">dashboard</i><span class="menu-title" data-i18n="">Home</span></a>
      </li>
      <li  class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/manage_item_type"><i style="color:#e7a922;" class="material-icons">menu</i><span class="menu-title" data-i18n="">Menu</span></a>
      </li>
      <li  class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/manageitems"><i style="color:#e7a922;" class="material-icons">restaurant</i><span class="menu-title" data-i18n="">Items</span></a>
      </li>
      <li  class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/manage_addons"><i style="color:#e7a922;" class="material-icons">extension</i><span class="menu-title" data-i18n="">Add Ons</span></a>
      </li>
      <li  class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/manageoptions"><i style="color:#e7a922;" class="material-icons">toc</i><span class="menu-title" data-i18n="">Options</span></a>
      </li>
      <li  class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/manage_cat_menu"><i style="color:#e7a922;" class="material-icons">merge_type</i><span class="menu-title" data-i18n="">Items Types</span></a>
      </li>
      <li  class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/managedeal"><i style="color:#e7a922;" class="material-icons">local_offer</i><span class="menu-title" data-i18n="">Offers</span></a>
      </li>
      <li  class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/client"><i style="color:#e7a922;" class="material-icons">perm_identity</i><span class="menu-title" data-i18n="">Users</span></a>
      </li>
      <li  class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/managecustomer"><i style="color:#e7a922;" class="material-icons">person</i><span class="menu-title" data-i18n="">Customers</span></a>
      </li>
      <li class="bold" >
         <a class="collapsible-header waves-effect waves-cyan " href="#"><i style="color:#e7a922;" class="material-icons">local_grocery_store</i><span class="menu-title" data-i18n="">Orders</span></a>
         <div class="collapsible-body">
            <ul>
               <li ><a class="collapsible-body" href="<?php echo base_url();?>admin/addorder" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Add Orders</span></a></li>
               <li ><a class="collapsible-body" href="<?php echo base_url();?>admin/manageorder" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Manage Orders</span></a></li>
               <li ><a class="collapsible-body" href="<?php echo base_url();?>admin/deliveredorder" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Delivered Orders</span></a></li>
               <li ><a class="collapsible-body" href="<?php echo base_url();?>admin/cancleorder" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Cancle Orders</span></a></li>
            </ul>
         </div>
      </li>
      <!-- product -->
      <li class="bold" >
      <li  class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/setting"><i style="color:#e7a922;" class="material-icons">settings</i><span class="menu-title" data-i18n="">Settings</span></a>
      </li>
   </ul>
   <div class="navigation-background"></div>
   <a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
</aside>
<!-- END: SideNav-->