<!-- <style>
   img
   	{
   		max-width:200px;
   		margin-top: 10px;
           display: content;
   	}
      </style> -->
      <div id="main">
   <div class="row">
      <div class="col s12">
         <div class="card">
            <div class="card-content">
               <div class="row">
                  <div class="col s12">
                     <?php echo form_open('admin/update_items');?>
                     <div class="col s12">
                        <!-- Form with placeholder -->
                        <h4 class="card-title">Update Products</h4>
                        <div class="row">
                           <div class="input-field col s12">
                              <input id="SKU" type="text" name="productsku" value="<?php echo $products['ProductSKU']; ?>">
                              <input type="hidden" value="<?php echo $products['ProductID']; ?>" name="ProductID" >
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <input id="name2" type="text" name="productname" value="<?php echo $products['ProductName']; ?>">
                           </div>
                        </div>
                        <!-- <div class="row">
                           <div class="input-field col s12">
                           <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $products['ProductImage']; ?>" value="<?php echo $products['ProductImage']; ?>" alt="avatar" style="width:100px; height:100px;">
                              <input  id="img2" type="file" name="userfile" >
                           </div>
                           </div> -->
                        <div class="row">
                           <div class="input-field col s12">
                              <input id="Price" type="text" name="productprice" value="<?php echo $products['ProductPrice']; ?>">
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <input id="Weight" type="text" name="productweight" value="<?php echo $products['ProductWeight']; ?>">
                           </div>
                        </div>
                        <div class="row">
                           <div class="col s12">
                              <label for="Category">Select Category *</label>
                              <div class="selected-box auto-hight">
                                 <select class="browser-default" name="categoryid" required>
                                    <option disabled>Select Category</option>
                                    <?php foreach ($categories as $category): ?>
                                    <?php if (empty($category['CategoryName'])) { }
                                       else{ if($category['CategoryID'] == $products['ProductCategoryID'] ){
                                             ?>
                                    <option value="<?php echo $category['CategoryID']; ?>" selected><?php echo $category['CategoryName']; ?></option>
                                    <?php
                                       }    else{
                                          ?>
                                    <option value="<?php echo $category['CategoryID']; ?>"><?php echo $category['CategoryName']; ?></option>
                                    <?php
                                       }    
                                       ?>
                                    <option value="<?php echo $category['CategoryID']; ?>"><?php echo $category['CategoryName']; ?></option>
                                    <?php }?>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <span class="helper-text"  data-success="right">Product Short Description</span>
                              <textarea style="height: 7rem;" name="productshortdesc" id="desc2" cols="30" rows="10"><?php echo $products['ProductShortDesc']; ?></textarea>
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <input id="Thumb" type="text" name="productthumb" value="<?php echo $products['ProductThumb']; ?>">
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <input id="Stock" type="text" name="productstock" value="<?php echo $products['ProductStock']; ?>">
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <input id="Live" type="text" name="productlive" value="<?php echo $products['ProductLive']; ?>">
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <input  id="Unlimited" type="text" name="productunlimited" value="<?php echo $products['ProductUnlimited']; ?>">
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <input  id="Location" type="text" name="productlocation" value="<?php echo $products['ProductLocation']; ?>">
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Update
                              <i class="material-icons right">send</i>
                              </button>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php echo form_close();?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>      
<script>
   $(document).ready(function(){
         $('.carousel').carousel();
   
         $('.datepicker').datepicker();
   
         $('.modal').modal();
   });
   
   
   
</script>
<script>
   function openModal(imageid,imagelink){
     // alert(imagelink);
     // alert(imageid);
   
     document.getElementById("modalimage").src = imagelink; 
     document.getElementById("deleteimage").href = "<?php echo base_url(); ?>admin/delete_product_picture/"+imageid+"?ProductID=<?php echo $products['ProductID']; ?>"; 
   
   
   
     $('#modal1').modal('open');
     // alert("sadasd");
     // alert(imageid );
   
   }
   
</script>
<script>
   function previewimg(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
   
              reader.onload = function (e) {
                  $('#view')
                      .attr('src', e.target.result);
              };
   
              reader.readAsDataURL(input.files[0]);
          }
      }
</script>