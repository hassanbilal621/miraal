
    <!-- BEGIN: Footer-->

    <footer class="page-footer footer footer-static footer-light navbar-border navbar-shadow">
      <div class="footer-copyright">
        <div class="container"><span>&copy; 2020 | All rights reserved.</span><span class="right hide-on-small-only">Design and Developed by <a>SOFTOLOGICS</a></span></div>
      </div>
    </footer>

    <!-- END: Footer-->
    <!-- BEGIN VENDOR JS-->
    <script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script> 
    <script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/app-assets/vendors/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/app-assets/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/app-assets/vendors/chartjs/chart.min.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/data-tables.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/extra-components-sweetalert.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/app-assets/js/scripts/advance-ui-carousel.js" type="text/javascript"></script>  
    <script src="<?php echo base_url();?>assets/app-assets/js/scripts/form-elements.js"></script>
    <script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
    <!-- <script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/dashboard-ecommerce.js" type="text/javascript"></script> -->
    <script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/charts-chartjs.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
  </body>
</html>        
        
        
        
