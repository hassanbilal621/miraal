<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Blogs</h4>
                  <div class="row">
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Blog Title</th>
                              <th>Blog</th>
                              <th>Blog Image</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($blogs as $blog):?>
                           <tr>
                              <td><?php echo $blog['blog_id'];?></td>
                              <td><?php echo $blog['blog_title'];?></td>
                              <td><?php echo $blog['blog_desc'];?></td>
                              <td><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $blog['blog_img'];?>" alt="avatar" style="width:100px; height:100px;"></td>
                              <td>
                                 <button id="<?php echo $blog['blog_id'];?>" onclick="loaduserinfo(this.id)" class="btn waves-effect waves-light blue btn" >Edit
                                 <i class="material-icons left">edit</i>
                                 </button>
                                 
                  <a href="<?php echo base_url(); ?>admin/del_blog/<?php echo $blog['blog_id']; ?>" class="btn waves-effect waves-light red" type="submit" name="action">Delete
                  <i class="material-icons left">delete_forever</i>
                  </a>
                  </td>
                  </tr>
                  <?php endforeach;?>
                  </tfoot>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>

<div id="modal2" class="modal">
                                    <div class="modal-content">
                                      
                                    </div>
                                 
                  </div>
                  <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>

<script type='text/javascript'>


   function loaduserinfo(Blogid){
      // var userid = this.id;
         $.ajax({
            type: "GET",
            url: "<?php echo base_url();?>admin/ajax_edit_Blogmodal/"+Blogid,
            success: function(data){
               $(".modal-content").html(data);
               $('#modal2').modal('open');
            }
         });
   }


</script>


<!-- BEGIN VENDOR JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->