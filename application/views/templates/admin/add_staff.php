<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <div class="row">
                     <div class="col s12">
                                       <?php echo form_open('admin/add_staff');?>
                                       <div class="col s12">
                                          <!-- Form with placeholder -->
                                          <h4 class="card-title">Create Staff</h4>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input style="height: 3rem; border: 1px solid #e7a922; border-radius:10px " id="name2" type="text" name="name">
                                                <label style="margin-left:10px;" for="name2">Staff Name </label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input style="height: 3rem; border: 1px solid #e7a922; border-radius:10px " id="email2" type="text" name="email">
                                                <label style="margin-left:10px;" for="email2">Email Address</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input style="height: 3rem; border: 1px solid #e7a922; border-radius:10px " id="phone2" type="text" name="phone">
                                                <label style="margin-left:10px;" for="phone2">Phone</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input style="height: 3rem; border: 1px solid #e7a922; border-radius:10px " id="password2" type="password" name="password">
                                                <label style="margin-left:10px;" for="password2">Password</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col s12">
                                                <label style="margin-left:10px;" for="Role">Select Role *</label>
                                                <div class="selected-box auto-hight">
                                                   <select class="form-control" name="roleid" required>
                                                      <option disabled selected>Select Role</option>
                                                      <?php foreach ($roles as $role): ?>
                                                      <?php if (empty($role['role'])) { }
                                                         else{        
                                                         ?>
                                                      <option value="<?php echo $role['role_id']; ?>"><?php echo $role['role']; ?></option>
                                                      <?php }?>
                                                      <?php endforeach; ?>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                                <i class="material-icons right">send</i>
                                                </button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php echo form_close();?>
                            
                     
                    
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- BEGIN VENDOR JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->


<div id="modal2" class="modal">
                                    <div class="modal-content">
                                 
                                    </div>
                                 </div>
                                 <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<script type='text/javascript'>
   function loaduserinfo(staffid){
      // var userid = this.id;
         $.ajax({
            type: "GET",
            url: "<?php echo base_url();?>admin/ajax_edit_staffmodal/"+staffid,
            success: function(data){
               $(".modal-content").html(data);
               $('#modal2').modal('open');
            }
         });
   }
   
   
</script>