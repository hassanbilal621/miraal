<div id="main">
   <div class="row">
   <div class="pt-1 pb-0" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s12 m6 l6">
                <h5 class="breadcrumbs-title">Manage Seller Data</h5>
              </div>
              <div class="col s12 m6 l6 right-align-md">
                <ol class="breadcrumbs mb-0">
                
                  <li class="breadcrumb-item active"><button class="waves-effect waves-light btn modal-trigger mb-2 mr-1" href="#modal1">Add Seller
                        <i class="material-icons right">shopping_basket</i>
                        </button>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <div class="row">
                     <div class="col s12">
                        
                        <div id="modal1" class="modal">
                           <div class="modal-content">
                              <div class="row">
                                 <div class="col s12">
                                    <div class="card">
                                       <?php echo form_open('admin/seller');?>
                                       <div class="col s12">
                                          <!-- Form with placeholder -->
                                          <h4 class="card-title">Create Seller</h4>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input id="name2" type="text" name="name">
                                                <label for="name2">Seller Name </label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input id="email2" type="text" name="email">
                                                <label for="email2">Email Address</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input id="password2" type="password" name="password">
                                                <label for="password2">Password</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                                <i class="material-icons right">send</i>
                                                </button>
                                             </div>
                                          </div>
                                    </div>
</div>
<?php echo form_close();?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Name</th>
                              <th>Email Address</th>
                              <th>Status</th>
                              <th>No. Of Product</th>
                              <th>Due To Seller</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($sellers as $seller):?>
                           <tr>
                              <td><?php echo $seller['seller_id'];?></td>
                              <td><?php echo $seller['name'];?></td>
                              <td><?php echo $seller['email'];?></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              </td>
                              <td>
                                 <button id="<?php echo $seller['seller_id'];?>" onclick="loaduserinfo(this.id)" class="btn waves-effect waves-light blue btn">Edit
                                 <i class="material-icons left">edit</i>
                                 </button>
                                
                                 <a href="<?php echo base_url(); ?>admin/del_seller/<?php echo $seller['seller_id']; ?>" class="btn waves-effect waves-light red" type="submit" name="action">Delete
                                 <i class="material-icons left">delete_forever</i>
                                 </a>
                              </td>
                           </tr>
                           <?php endforeach;?>
                           </tfoot>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>


<div id="modal2" class="modal">
                                    <div class="modal-content">
                                       
                                    </div>
                                 </div>
                                 <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>

<script type='text/javascript'>


   function loaduserinfo(sellerid){
      // var userid = this.id;
         $.ajax({
            type: "GET",
            url: "<?php echo base_url();?>admin/ajax_edit_sellermodal/"+sellerid,
            success: function(data){
               $(".modal-content").html(data);
               $('#modal2').modal('open');
            }
         });
   }


</script>



<!-- BEGIN VENDOR JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->