<div id="main">
   <div class="row">
      <div class="pt-1 pb-0" id="breadcrumbs-wrapper">
         <!-- Search for small screen-->
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title">Manage Menu</h5>
               </div>
               <div class="col s12 m6 l6 right-align-md">
                  <ol class="breadcrumbs mb-0">
                     <li><a class="btn green" href="<?php echo base_url();?>admin/item_type">
                        <i class="material-icons center">add</i>
                        </a>
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </div>
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <table id="page-length-option" class="display">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>Name</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <?php foreach ($types as $type): ?>
                           <td><?php echo $type['item_type_id']; ?></td>
                           <td><?php echo $type['item_type_name']; ?></td>
                           
                           <td>
                              <button id="<?php echo $type['item_type_id']; ?>"  onclick="loaduserinfo(this.id)" class="btn waves-effect waves-light blue btn">Edit
                              <i class="material-icons left">edit</i>
                              </button>
                              <a href="<?php echo base_url(); ?>admin/del_itemtype/<?php echo $type['item_type_id']; ?>" class="btn waves-effect waves-light red" type="submit" name="action">Delete
                              <i class="material-icons left">delete_forever</i>
                              </a>
                           </td>
                        </tr>
                        <?php endforeach; ?>
                        </tfoot>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<!-- BEGIN VENDOR JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<div id="modal2" class="modal">
   <div class="modal-content">
   </div>
</div>
<script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<script type='text/javascript'>
   function loaduserinfo(type_id){
      // var userid = this.id;
         $.ajax({
            type: "GET",
            url: "<?php echo base_url();?>admin/ajax_edit_itemtypemodal/"+type_id,
            success: function(data){
               $(".modal-content").html(data);
               $('#modal2').modal('open');
            }
         });
   }
   
   
</script>