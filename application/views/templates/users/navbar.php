<header class="page-topbar" id="header">
   <div class="navbar navbar-fixed">
      <nav class="navbar-main">
         <div class="container" style="width:1200px;">
            <div class="nav-wrapper">
               <ul class="left">
                  <li>
                     <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="<?php echo base_url(); ?>users/index"><img src="<?php echo base_url(); ?>assets/app-assets/images/logo/Miraal.png" alt=" logo" style="margin-bottom:-10px; height: 40px; margin-top:-10px" ;><span class="logo-text hide-on-med-and-down"></span></a></h1>
                  </li>
               </ul>
               <ul class="navbar-list right">

                  
               <li><a class="waves-effect " href="javascript:void(0);" style="color: black;">Menu</a></li>
               <li><a class="waves-effect " href="javascript:void(0);" style="color: black;">About Us</a></li>
               <li><a class="waves-effect " href="javascript:void(0);" style="color: black;">Contact Us</a></li>
                  <li><a class="waves-effect  waves-light shopping-button" href="<?php echo base_url(); ?>users/cart" data-target="Shopping-dropdown"><span class="avatar-status avatar-online"><img src="<?php echo base_url(); ?>assets/app-assets/images/icon/cart.png" alt="avatar" style="border-radius: 0;background: no-repeat;"><small class="notification-badge orange accent-3"><?php echo $this->cart->total_items(); ?></small></span></a></li>
                  <li><a class="waves-effect   waves-dark profile-button" href="javascript:void(0);" data-target="profile-dropdown"><span class="avatar-status avatar-online"><img src="<?php echo base_url(); ?>assets/app-assets/images/icon/proicon.png" alt="avatar" style="border-radius: 0;background: no-repeat;"></span></a></li>
                  <li><a href="#" data-target="contact-sidenav" style="color: black;" class="sidenav-trigger hide-on-large-only"><i class="material-icons">menu</i></a></li>
               </ul>
               <!-- profile-dropdown-->
               <ul class="dropdown-content" id="profile-dropdown">
                  <?php if (!$this->session->userdata('apna_user_id')) { ?>
                     <li><a class="grey-text text-darken-1" href="<?php echo base_url(); ?>users/login"><i class="material-icons">person_outline</i> login</a></li>
                     <li><a class="grey-text text-darken-1" href="<?php echo base_url(); ?>blogs"><i class="material-icons">event_note</i> Blogs</a></li>
                     <li><a class="grey-text text-darken-1" href="<?php echo base_url(); ?>shops/"><i class="material-icons">shop</i> Shop</a></li>
                     <li><a class="grey-text text-darken-1" href="<?php echo base_url(); ?>seller/productadd"><i class="material-icons">shop</i> Seller</a></li>
                  <?php } else { ?>
                     <li><a class="grey-text text-darken-1" href="<?php echo base_url(); ?>seller/productadd"><i class="material-icons">shop</i> Seller</a></li>
                     <li><a class="grey-text text-darken-1" href="<?php echo base_url(); ?>blogs"><i class="material-icons">event_note</i> Blogs</a></li>
                     <li><a class="grey-text text-darken-1" href="<?php echo base_url(); ?>shops/"><i class="material-icons">shop</i> Shop</a></li>
                     <li><a class="grey-text text-darken-1" href="<?php echo base_url(); ?>users/profile"><i class="material-icons">person_outline</i>Profile</a></li>
                     <li><a class="grey-text text-darken-1" href="<?php echo base_url(); ?>users/logout"><i class="material-icons"><img src="<?php echo base_url(); ?>assets/app-assets/images/icon/logout.ico" style="height: 30px;margin: -5px -15px 0 0;"> </i> Logout</a></li>
                     </li> <?php } ?>
               </ul>
            </div>
         
         </div>
      </nav>
   </div>
</header>
<!-- 
   <div class="nav-wrapper">
   					<ul class="left">
   						<li>
   							<h1 class="logo-wrapper"><a class="brand-logo darken-1" href="<?php echo base_url(); ?>users/index"><img src="<?php echo base_url(); ?>assets/app-assets/images/logo/kalakaar logo.png" alt=" logo"style="margin-bottom:-15px; height: 50px; margin-top:-15px";><span class="logo-text hide-on-med-and-down" style="color:black"; >Apna Kalakaar</span></a></h1>
   						</li>
   					</ul>
   					<div class="header-search-wrapper">
   					</div>
   					<ul class="navbar-list right">
   						<li><a class="waves-effect waves-block waves-light search-button" href="javascript:void(0);"style="color: black;"><i class="material-icons">search</i></a></li>
   						<li><a class="waves-effect  waves-light shopping-button" href="<?php echo base_url(); ?>users/cart" data-target="Shopping-dropdown"><span class="avatar-status avatar-online"><img src="<?php echo base_url(); ?>assets/app-assets/images/icon/cart.png" alt="avatar"style="border-radius: 0;background: no-repeat;"><small class="notification-badge orange accent-3"><?php echo $this->cart->total_items(); ?></small></span></a></li>
   						<li><a class="waves-effect   waves-dark profile-button" href="javascript:void(0);" data-target="profile-dropdown"><span class="avatar-status avatar-online"><img src="<?php echo base_url(); ?>assets/app-assets/images/icon/proicon.png" alt="avatar"style="border-radius: 0;background: no-repeat;"></span></a></li>
   					</ul>
   					</ul>
   					
   					<ul class="dropdown-content" id="profile-dropdown">
   						<?php if (!$this->session->userdata('apna_user_id')) { ?> 
   						<li><a class="grey-text text-darken-1" href="<?php echo base_url(); ?>users/login"><i class="material-icons">person_outline</i> login</a></li>
   						<li><a class="grey-text text-darken-1" href="<?php echo base_url(); ?>users/shop"><i class="material-icons">shop</i> Shop</a></li>
   						<?php } else { ?>
   						<li><a class="grey-text text-darken-1" href="<?php echo base_url(); ?>users/shop"><i class="material-icons">shop</i> Shop</a></li>
   						<li><a class="grey-text text-darken-1" href="<?php echo base_url(); ?>users/profile"><i class="material-icons">person_outline</i>Profile</a></li>
   						<li><a class="grey-text text-darken-1" href="<?php echo base_url(); ?>users/logout"><i class="material-icons"><img src="<?php echo base_url(); ?>assets/app-assets/images/icon/logout.ico" style="height: 30px;margin: -5px -15px 0 0;"> </i> Logout</a></li>
   						</li> <?php } ?>
   					</ul>
   					<div class="display-none search-sm">
   						<div class="input-field">
   							<input class="search-box-sm header-search-input z-depth-2" type="search" name="Search" placeholder="search" required="" style="border: 1px black solid;background: white;border-radius: 25px;width: 300px;margin: -65px 170px 0px 0;height: 25px;float: right;">
   						</div>
   					</div>
   				</div> -->