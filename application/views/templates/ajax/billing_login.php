<?php echo form_open('users/login');?>
                     <div class="login-form" >
                        <div class="row">
                           <div class="input-field col s12">
                              <center><img src="<?php echo base_url(); ?>assets/app-assets/images\logo/kalakaar logo.png" alt=""style="height: 150px; margin: auto;"></center>
                           </div>
                        </div>
                        <div class="row margin">
                           <div class="input-field col s12">
                              <i class="material-icons prefix pt-2">person_outline</i>
                              <input id="username" type="text" name="username">
                              <label for="username" class="center-align">Email</label>
                           </div>
                        </div>
                        <div class="row margin">
                           <div class="input-field col s12">
                              <i class="material-icons prefix pt-2">lock_outline</i>
                              <input id="password" type="password" name="password">
                              <label for="password">Password</label>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col s12 m12 l12 ml-2 mt-1">
                              <p>
                                 <label>
                                 <input type="checkbox" />
                                 <span>Remember Me</span>
                                 </label>
                              </p>
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <button type="submit" class="btn waves-effect waves-light border-round cyan col s12">Login</button>
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s6 m6 l6">
                              <p class="margin medium-small"><a href="regist">Register Now!</a></p>
                           </div>
                           <div class="input-field col s6 m6 l6">
                              <p class="margin right-align medium-small"><a href="user-forgot-password.html">Forgot password ?</a></p>
                           </div>
                        </div>
                     </div>
                     <?php echo form_close();?>