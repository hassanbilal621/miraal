<div class="row">
   <div class="col s12">
      <div class="card">
         <?php echo form_open_multipart('admin/update_addons');?>
         <div class="col s12">
            <!-- Form with placeholder -->
            <h4 class="card-title">Update Menu Category</h4>
            <div class="row">
               <div class="input-field col s12">
                  <input id="name2" type="text" name="add_ons_name" value="<?php echo $addons['add_ons_name']?>">
                  <input type="hidden" value="<?php echo $addons['add_ons_id']; ?>" name="add_ons_id" >
                  <label for="name2">Name </label>
               </div>
            </div>
            <div class="row">
               <div class="input-field col s12">
                  <input id="name2" type="text" name="add_ons_price" value="<?php echo $addons['add_ons_price']?>">
                  <label for="name2">Name </label>
               </div>
            </div>
            <div class="row">
               <div class="input-field col s12">
                  <span for="img2">Category Image</span>
                  <input  id="img2" type="file" name="userfile" >
                  <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $addons['add_ons_img']; ?>" value="<?php echo $addons['add_ons_img']?>" alt="avatar" style="width:100px; height:100px;">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s12">
                  <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Save
                  <i class="material-icons right">send</i>
                  </button>
               </div>
            </div>
         </div>
      </div>
      <?php echo form_close();?>
   </div>
</div>