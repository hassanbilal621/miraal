<div class="row">
   <div class="col s12">
      <div class="card">
         <?php echo form_open('admin/update_itemtype');?>
         <div class="col s12">
            <!-- Form with placeholder -->
            <h4 class="card-title">Update Product Category</h4>
            <div class="row">
               <div class="input-field col s12">
                  <input id="name2" type="text" name="item_type_name" value="<?php echo $types['item_type_name']?>">
                  <input type="hidden" value="<?php echo $types['item_type_id']; ?>" name="item_type_id" >
                  <label for="name2">Name </label>
               </div>
            </div>
            <div class="row">
               <div class="input-field col s12">
                  <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Save
                  <i class="material-icons right">send</i>
                  </button>
               </div>
            </div>
         </div>
      </div>
      <?php echo form_close();?>
   </div>
</div>