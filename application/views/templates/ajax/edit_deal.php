<div class="row">
   <div class="col s12">
      <div class="card">
         <?php echo form_open_multipart('admin/update_deal');?>
         <div class="col s12">
            <!-- Form with placeholder -->
            <h4 class="card-title">Update Menu Category</h4>
            <div class="row">
               <div class="input-field col s12">
                  <input id="name2" type="text" name="deal_name" value="<?php echo $deals['deal_name']?>">
                  <input type="hidden" value="<?php echo $deals['deal_id']; ?>" name="deal_id" >
                  <label for="name2">Name </label>
               </div>
            </div>
            <div class="row">
                              <div class="input-field col s12">
                                 <input style="height: 3rem; border: 1px solid #e7a922; border-radius:10px " id="price2" type="text" value="<?php echo $deals['deal_price']?>" name="deal_price">
                                 <label style="margin-left:10px;" for="price2">Price</label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="input-field col s12">
                                 <input style="height: 3rem; border: 1px solid #e7a922; border-radius:10px " id="desc2" type="text" value="<?php echo $deals['deal_description']?>" name="deal_description">
                                 <label style="margin-left:10px;" for="desc2">Description</label>
                              </div>
                           </div>
            <div class="row">
               <div class="input-field col s12">
                  <span for="img2">Category Image</span>
                  <input  id="img2" type="file" name="userfile" >
                  <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $deals['img']; ?>" value="<?php echo $deals['img']?>" alt="avatar" style="width:100px; height:100px;">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s12">
                  <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Save
                  <i class="material-icons right">send</i>
                  </button>
               </div>
            </div>
         </div>
      </div>
      <?php echo form_close();?>
   </div>
</div>