<?php echo form_open('admin/addproduct') ?>

<div class="row" id="product-one">
      
      <div class="col s12">
        <a class="modal-close right"><i class="material-icons">close</i></a>
      </div>
      
      <div class="col m6">
      <img src="<?php echo base_url(); ?>assets/app-assets/images/cards/cameras.png" class="responsive-img" alt="">
      <!-- <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $productimg['product_img']; ?>" alt="avatar" style="width:100px; height:100px;"> -->
        
      </div>
      
      <div class="col m6">
        <p>Category : <?php echo $item['item_type_name']; ?> </p>
        <h5><?php echo $item['item_name']; ?></h5>
        <input type="hidden" value="<?php echo $item['item_id']; ?>" name="productid" />
        <input type="hidden" value="<?php echo $item['item_name']; ?>" name="productname" />
        <input type="hidden" value="<?php echo $item['item_price']; ?>" name="productprice" />
        <hr class="mb-5">
        <!-- <span class="vertical-align-top mr-4"><i class="material-icons mr-3">favorite_border</i>Brand : <?php echo $item['brand_name']; ?> </span> -->
        <p>Qty : <input style="width:10%;" class="" type="number" value="1" name="productqty" required/></p>
        
        <h5><span class="prise-text-style ml-2">$ <?php echo $item['item_price']; ?></span></h5>
        <button class="waves-effect waves-light btn gradient-45deg-deep-purple-blue mt-2 right" type="submit">ADD TO CART</button>
      </div>
      
    </div>
    
    <?php echo form_close() ?>
