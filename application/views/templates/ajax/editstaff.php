<div class="row">
                                          <div class="col s12">
                                             <div class="card">
                                             <?php echo form_open('admin/updatestaff');?>
                                                <div class="col s12">
                                                   <!-- Form with placeholder -->
                                                   <h4 class="card-title">Edit Staffs</h4>
                                                   <div class="row">
                                                      <div class="input-field col s12">
                                                         <input id="name2" type="text" name="name" value="<?php echo $Staffs['name'];?>">
                                                         <input type="hidden" value="<?php echo $Staffs['staff_id']; ?>" name="staff_id" >
                                                        
                                                      </div>
                                                   </div>
                                                   <div class="row">
                                                      <div class="input-field col s12">
                                                         <input id="email2" type="text" name="email" value="<?php echo $Staffs['email'];?>">
                                                         
                                                      </div>
                                                   </div>
                                                   <div class="row">
                                                      <div class="input-field col s12">
                                                         <input id="phone2" type="text" name="phone" value="<?php echo $Staffs['phone'];?>">
                                                        
                                                      </div>
                                                   </div>
                                                  
                                                   <div class="row">
                                                <div class="col s12">
                                                        <label for="Category">Select Role *</label>
                                                        <div class="selected-box auto-hight">
                                                            <select class="browser-default" name="roleid" required>
                                                            <option disabled>Select Role</option>
                                                            <?php foreach ($roles as $role): ?>
                                                            <?php if (empty($role['role'])) { }
                                                                else{ if($role['role_id'] == $Staffs['roleid'] ){
                                                                        ?>
                                                            <option value="<?php echo $role['role_id']; ?>" selected><?php echo $role['role']; ?></option>
                                                            <?php
                                                                }    else{
                                                                    ?>
                                                            <option value="<?php echo $role['role_id']; ?>"><?php echo $role['role']; ?></option>
                                                            <?php
                                                                }    
                                                                ?>
                                                            <option value="<?php echo $role['role_id']; ?>"><?php echo $role['role']; ?></option>
                                                            <?php }?>
                                                            <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    </div>

                                                  
                                                   <div class="row">
                                                      <div class="input-field col s12">
                                                         <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Save
                                                         <i class="material-icons right">send</i>
                                                         </button>
                                                      </div>
                                                   </div>
                                             </div>
                                             </div>
                                             <?php echo form_close();?>
                                          </div>
                                       </div>