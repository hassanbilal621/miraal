<div class="row">
   <div class="col s12">
      <div class="card">
         <?php echo form_open('admin/update_options');?>
         <div class="col s12">
            <!-- Form with placeholder -->
            <h4 class="card-title">Update Options</h4>
            <div class="row">
               <div class="input-field col s12">
                  <input id="name2" type="text" name="OptionName" value="<?php echo $options['OptionName']?>">
                  <input type="hidden" value="<?php echo $options['OptionID']; ?>" name="OptionID" >
                  <label for="name2">Name </label>
               </div>
            </div>
            <div class="row">
               <div class="input-field col s12">
                  <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Save
                  <i class="material-icons right">send</i>
                  </button>
               </div>
            </div>
         </div>
      </div>
      <?php echo form_close();?>
   </div>
</div>