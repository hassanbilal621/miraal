<div class="row">
                  <div class="col s12">
                     <?php echo form_open_multipart('admin/update_items');?>
                     <div class="col s12">
                        <!-- Form with placeholder -->
                        <h4 class="card-title">Update Items</h4>
                        <div class="row">
                           <div class="input-field col s12">
                              <input id="SKU" style="height: 3rem; border: 1px solid #e7a922; border-radius:10px " type="text" name="item_name" value="<?php echo $Items['item_name']; ?>">
                              <input type="hidden" value="<?php echo $Items['item_id']; ?>" name="item_id" >
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <input id="name2" style="height: 3rem; border: 1px solid #e7a922; border-radius:10px " type="text" name="item_price" value="<?php echo $Items['item_price']; ?>">
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                           <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $Items['item_image']; ?>" value="<?php echo $Items['item_image']; ?>" alt="avatar" style="width:100px; height:100px;">
                              <input  id="img2" type="file" name="userfile" >
                           </div>
                           </div>
                       
                        <div class="row">
                           <div class="col s12">
                              <label style="margin-left:10px;" for="menutype">Select Menu type *</label>
                              <div style="height: 3rem; border: 1px solid #e7a922; border-radius:10px" class="selected-box auto-hight">
                                 <select class="browser-default" name="menu_id" required>
                                    <option disabled>Select type</option>
                                    <?php foreach ($menus as $menu): ?>
                                    <?php if (empty($menu['name'])) { }
                                       else{ if($menu['item_menu_id'] == $Items['menu_cat_id'] ){
                                             ?>
                                    <option value="<?php echo $menu['cat_menu_id']; ?>" selected><?php echo $menu['name']; ?></option>
                                    <?php
                                       }    else{
                                          ?>
                                    <option value="<?php echo $menu['cat_menu_id']; ?>"><?php echo $menu['name']; ?></option>
                                    <?php
                                       }    
                                       ?>
                                    <option value="<?php echo $menu['cat_menu_id']; ?>"><?php echo $menu['name']; ?></option>
                                    <?php }?>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col s12">
                              <label style="margin-left:10px;" for="type">Select type *</label>
                              <div style="height: 3rem; border: 1px solid #e7a922; border-radius:10px" class="selected-box auto-hight">
                                 <select class="browser-default" name="type_id" required>
                                    <option disabled>Select type</option>
                                    <?php foreach ($types as $type): ?>
                                    <?php if (empty($type['item_type_name'])) { }
                                       else{ if($type['item_type_id'] == $Items['item_type_id'] ){
                                             ?>
                                    <option value="<?php echo $type['item_type_id']; ?>" selected><?php echo $type['item_type_name']; ?></option>
                                    <?php
                                       }    else{
                                          ?>
                                    <option value="<?php echo $type['item_type_id']; ?>"><?php echo $type['item_type_name']; ?></option>
                                    <?php
                                       }    
                                       ?>
                                    <option value="<?php echo $type['item_type_id']; ?>"><?php echo $type['item_type_name']; ?></option>
                                    <?php }?>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                       
                        <div class="row">
                           <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Update
                              <i class="material-icons right">send</i>
                              </button>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php echo form_close();?>
               </div>