<?php
class Shops extends CI_Controller{

	public function __construct()
    {
	  parent::__construct();
	  $this->load->library('cart');
	  $this->load->model('user_model');
	}
    
    
    public function index(){


        $data['products'] = $this->admin_model->get_product();
		$data['productimgs'] = $this->admin_model->get_image();
        $data['categories'] = $this->admin_model->get_cat();
        $data['sellers'] = $this->admin_model->get_seller();
        $data['title'] = "All Blogs";

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/aside.php', $data);
        $this->load->view('templates/shops/index.php', $data);
        $this->load->view('templates/users/footer.php');


    }

    public function view($sellerid){


        // $data['products'] = $this->admin_model->get_product();
        $data['products'] = $this->admin_model->get_sellerproduct($sellerid);

		$data['productimgs'] = $this->admin_model->get_image();
        $data['categories'] = $this->admin_model->get_cat();
        $data['sellers'] = $this->admin_model->get_seller();
        $data['title'] = "All Blogs";

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/aside.php', $data);
        $this->load->view('templates/shops/view.php', $data);
        $this->load->view('templates/users/footer.php');


    }


    






}