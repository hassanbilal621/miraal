<?php
class Seller extends CI_Controller{

	public function __construct()
    {
	  parent::__construct();
	  $this->load->library('cart');
	  $this->load->model('user_model');
	  $this->load->model('seller_model');
	}
	

	public function login(){
		if($this->session->userdata('apna_seller_id')){
            redirect('seller/');
        }
		
   
        $this->form_validation->set_rules('username', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		// echo  $this->input->post('username');
		// echo '<br>';
		// echo $this->input->post('password');
		// die;

        if($this->form_validation->run() === FALSE){
			// $username = $this->input->post('username');
			// $password = $this->input->post('password');
	
		// echo  $this->input->post('username');
		// echo '<br>';
		// echo $this->input->post('password');
		// die;

		// 	die;
		$data['title'] = 'apna kalakar = Login';
            
            $this->load->view('templates/users/header.php');
            $this->load->view('templates/seller/login.php', $data);
            $this->load->view('templates/users/footer.php');


        } 
        else {

			
            $username = $this->input->post('username');
			$password = $this->input->post('password');

			$user_id = $this->seller_model->login($username, $password);


			// echo $user_id;
			// die;
			//$employee_id = $this->employee_model->login($username, $password);

			if($user_id){
                $user_data = array(
                    'apna_seller_id' => $user_id,
                    'username' => $username,
                    'apna_logged_in' => true
                );
                $this->session->set_userdata($user_data);
				
				
				//set cookie for 1 year
				$cookie = array(
					'name'   => 'apna_seller_id',
					'value'  => $user_id,
					'expire' => time()+31556926
				);
				$this->input->set_cookie($cookie);

				
                $this->session->set_flashdata('user_loggedin', 'You are now logged in');
                redirect('seller/');
			} 
	
            else {

                $this->session->set_flashdata('login_failed', 'Login is invalid. Incorrect username or password.');
                redirect('seller/login');
            }		
        }
	}


	public function logout(){

        $this->session->unset_userdata('apna_seller_id');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('necxo_logged_in');
		
		delete_cookie('jobboard_user_id');

        $this->session->set_flashdata('user_loggedout', 'You are now logged out');
        redirect('seller/login');
	}
	



    public function register()
	{
		// if(!$this->session->userdata('apna_seller_id'))
		// {
		// 	redirect('seller/login');
		// }

		$this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique[seller.email]');
        $this->form_validation->set_rules('password', 'password', 'required');
		
		if($this->form_validation->run() === FALSE){

		$this->load->view('templates/seller/header.php');
        $this->load->view('templates/seller/navbar.php');
        $this->load->view('templates/seller/register.php');
		$this->load->view('templates/seller/footer.php');
		}
		else{
			$enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            //$enc_password = md5($this->input->post('password'));
            $this->seller_model->register($enc_password);

			$this->session->set_flashdata('user_registered', 'You are sucessfully registered');
			
            redirect('seller/login');
            
        }
	}
	
	
    public function index()
	{
		// if(!$this->session->userdata('apna_seller_id'))
		// {
		// 	redirect('seller/login');
		// }
		

		$this->load->view('templates/seller/header.php');
        $this->load->view('templates/seller/navbar.php');
        $this->load->view('templates/seller/aside.php');
        $this->load->view('templates/seller/index.php');
        $this->load->view('templates/seller/footer.php');
    }


    public function seller()
	{
		if(!$this->session->userdata('apna_seller_id'))
		{
			redirect('seller/login');
		}
		

		$this->load->view('templates/seller/header.php');
        $this->load->view('templates/seller/navbar.php');
        $this->load->view('templates/seller/aside.php');
        $this->load->view('templates/seller/seller.php');
        $this->load->view('templates/seller/footer.php');
    }

    public function productadd()
	{
		if(!$this->session->userdata('apna_seller_id'))
		{
			redirect('seller/login');
		}
		$this->form_validation->set_rules('productsku', 'productsku', 'required');
        if($this->form_validation->run() === FALSE){
            $data['categories'] = $this->admin_model->get_cat();
            $data['products'] = $this->admin_model->get_product();
            $data['brands'] = $this->admin_model->get_brand();

		$this->load->view('templates/seller/header.php');
        $this->load->view('templates/seller/navbar.php');
        $this->load->view('templates/seller/aside.php');
        $this->load->view('templates/seller/productadd.php', $data);
		$this->load->view('templates/seller/footer.php');
	}
	else{

		$this->admin_model->add_product();
		redirect('Seller/productadd');
	}
    }

    public function orders()
	{
		if(!$this->session->userdata('apna_seller_id'))
		{
			redirect('seller/login');
		}
		

		$this->load->view('templates/seller/header.php');
        $this->load->view('templates/seller/navbar.php');
        $this->load->view('templates/seller/aside.php');
        $this->load->view('templates/seller/orders.php');
        $this->load->view('templates/seller/footer.php');
    }

    public function pendingorders()
	{
		if(!$this->session->userdata('apna_seller_id'))
		{
			redirect('seller/login');
		}
		

		$this->load->view('templates/seller/header.php');
        $this->load->view('templates/seller/navbar.php');
        $this->load->view('templates/seller/aside.php');
        $this->load->view('templates/seller/pendingorders.php');
        $this->load->view('templates/seller/footer.php');
    }

    public function deliveredorders()
	{
		if(!$this->session->userdata('apna_seller_id'))
		{
			redirect('seller/login');
		}
		

		$this->load->view('templates/seller/header.php');
        $this->load->view('templates/seller/navbar.php');
        $this->load->view('templates/seller/aside.php');
        $this->load->view('templates/seller/deliveredorders.php');
        $this->load->view('templates/seller/footer.php');
    }

}