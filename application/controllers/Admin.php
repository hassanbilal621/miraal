<?php
class admin extends CI_Controller{
    public function __construct()
    {
      parent::__construct();
      $this->load->library('cart');
      $this->load->model('admin_model');
      $this->load->model('blog_model');
    }

    public function login(){

        if($this->session->admindata('restaurant_admin_id')){
            redirect('admin/');
        }

        $data['title'] = 'Nexco Japan';

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if($this->form_validation->run() === FALSE){


            $this->load->view('templates/admin/login.php', $data);
        }else{

            $username = $this->input->post('username');
            $password = $this->input->post('password');

            //$user_id = $this->admin_model->login($username, $password);
            $restaurant_admin_id = $this->admin_model->login($username, $password);

            if($restaurant_admin_id){
                $admin_data = array(
                    'restaurant_admin_id' => $restaurant_admin_id,
                    'japan_email' => $username,
                    'submit_alogged_in' => true
                );
                $this->session->set_admindata($admin_data);
				
                redirect('admin/');
			} 
	
            else {

                $this->session->set_flashdata('login_failed', 'Login is invalid. Incorrect username or password.');
                redirect('admin/login');
            }	

        }

    }


    public function logout(){

        $this->session->unset_userdata('restaurant_admin_id');
        $this->session->unset_userdata('japan_email');
        $this->session->unset_userdata('submit_alogged_in');
		
        $this->session->set_flashdata('admin_loggedout', 'You are now logged out');
        redirect('admin/login');
    }





    public function index(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
		}
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/index.php');
        $this->load->view('templates/admin/footer.php');

    }

    public function ajax_edit_itemtypemodal($type_id){
        
        $data['types'] = $this->admin_model->get_itemtypeinfo($type_id);

        $this->load->view('templates/ajax/edit_itemtype.php', $data);
    }

    public function ajax_edit_itemsmodal($item_id){

        $data['Items'] = $this->admin_model->get_itemsinfo($item_id);
        $data['types'] = $this->admin_model->get_itemtype($item_id);
        $data['menus'] = $this->admin_model->get_cat_menu($item_id);

        $this->load->view('templates/ajax/edit_items.php', $data);
    }

    public function item_type(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }
        
        $this->form_validation->set_rules('item_type_name', 'item_type_name', 'required');
        if($this->form_validation->run() === FALSE){
        
            $data['types'] = $this->admin_model->get_itemtype();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/item_type.php', $data);
        $this->load->view('templates/admin/footer.php');
        }
        else{

            $this->admin_model->add_itemtype();
            redirect('admin/item_type');
        }
    }

    public function manage_item_type(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }
        
       
        if($this->form_validation->run() === FALSE){
        
            $data['types'] = $this->admin_model->get_itemtype();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/manage_item_type.php', $data);
        $this->load->view('templates/admin/footer.php');
        }
        else{

            $this->admin_model->update_itemtype();
            redirect('admin/manage_item_type');
        }
    }

    public function del_itemtype($type_id){

        $this->admin_model->del_itemtype($type_id);
        redirect('admin/manage_item_type');
        
    }

    public function del_subcat($subcatid){

        $this->admin_model->del_subcat($subcatid);
        redirect('admin/managesubcategory');
        
    }

   

    public function del_seller($sellerid){

        $this->admin_model->del_seller($sellerid);
        redirect('admin/seller');
        
    }

    public function del_staff($staffid){

        $this->admin_model->del_staff($staffid);
        redirect('admin/administrator');
        
    }

    public function del_role($roleid){

        $this->admin_model->del_role($roleid);
        redirect('admin/role');
        
    }

    public function update_itemtype(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
		}
		
        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('item_type_name', 'item_type_name', 'required');
		


        if($this->form_validation->run() === FALSE){
       

			$this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/Type?fail');
        }
        else{

            $this->admin_model->update_itemtype(); 
			
			$this->session->set_flashdata('user_registered', 'You are sucessfully registered');

		
            redirect('admin/manage_item_type');
            
        }
  
    }
    
    
    

    public function ajax_edit_subcategorymodal($subcatid){

        $data['subcategories'] = $this->admin_model->get_subcategoryinfo($subcatid);
        $data['categories'] = $this->admin_model->get_cat();
        $this->load->view('templates/ajax/edit_subcategory.php', $data);
    }
    

    public function update_subcat(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
		}
		
        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('subcate_name', 'subcate_name', 'required');
		
        

        if($this->form_validation->run() === FALSE){
            $data['categories'] = $this->admin_model->get_cat();

			$this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/subcategory?fail');
        }
        else{

            $this->admin_model->update_subcat();
			
			$this->session->set_flashdata('user_registered', 'You are sucessfully registered');

		
            redirect('admin/managesubcategory');
            
        }
  
    }
    
    public function subcategory(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }
        
        $this->form_validation->set_rules('subcate_name', 'subcate_name', 'required');
        if($this->form_validation->run() === FALSE){

        $data['categories'] = $this->admin_model->get_cat();
        $data['subcategories'] = $this->admin_model->get_subcat();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/subcategory.php', $data);
        $this->load->view('templates/admin/footer.php');
        }
        else{

            $this->admin_model->add_subcat();
            redirect('admin/subcategory');
        }
    }

    public function managesubcategory(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }
        
        $this->form_validation->set_rules('subcate_name', 'subcate_name', 'required');
        if($this->form_validation->run() === FALSE){

        $data['categories'] = $this->admin_model->get_cat();
        $data['subcategories'] = $this->admin_model->get_subcat();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/managesubcategory.php', $data);
        $this->load->view('templates/admin/footer.php');
        }
        else{

            $this->admin_model->update_subcat();
            redirect('admin/subcategory');
        }
    }


    public function add_items(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }
        
        $this->form_validation->set_rules('item_name', 'item_name', 'required');
        if($this->form_validation->run() === FALSE){
            $data['types'] = $this->admin_model->get_itemtype();
            $data['items'] = $this->admin_model->get_items();
            $data['menus'] = $this->admin_model->get_cat_menu();
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/add_items.php', $data);
        $this->load->view('templates/admin/footer.php'); 
        }
        else{
            $imgname = $this->do_upload();
            $this->admin_model->add_items($imgname);
            redirect('admin/add_items');
        }
    }

    
    public function manageitems(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }
        if($this->form_validation->run() === FALSE){
            $data['types'] = $this->admin_model->get_itemtype();
            $data['items'] = $this->admin_model->get_items();
            $data['menus'] = $this->admin_model->get_cat_menu();
            
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/manageitems.php', $data);
        $this->load->view('templates/admin/footer.php'); 
        }
        else{
            $imgname = $this->do_upload();
            $this->admin_model->update_items($imgname);
            redirect('admin/manageitems');
        }
    }

    public function del_items($item_id){

        $this->admin_model->del_items($item_id);
        redirect('admin/manageitems');
        
    }

    public function edit_items($item_id){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }
        
        if($this->form_validation->run() === FALSE){
        $data['categories'] = $this->admin_model->get_cat($item_id);
        $data['items'] = $this->admin_model->get_edititems($item_id);
        $data['productimgs'] = $this->admin_model->get_product_image($item_id);
        $data['menus'] = $this->admin_model->get_cat_menu($item_id);

        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/edit_product.php', $data);
        $this->load->view('templates/admin/footer.php'); 
        }
        else{
            $imgname = $this->do_upload();
            $this->admin_model->update_items($imgname);
            redirect('admin/manageitems');
        }
    }

    // public function update_product(){
    //     if(!$this->session->admindata('restaurant_admin_id'))
	// 	{
	// 		redirect('admin/login');
    //     }
    //     $item_id = $this->input->post('ProductID');
		
    //     $this->admin_model->update_product($item_id);
    //     redirect('admin/manageitems/'.$item_id);
  
    // }

    public function update_items(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
		}
		
        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('item_name', 'item_name', 'required');
		


        if($this->form_validation->run() === FALSE){
       

			$this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/Items?fail');
        }
        else{

            $imgname = $this->do_upload();
            $this->admin_model->update_items($imgname); 
			
			$this->session->set_flashdata('user_registered', 'You are sucessfully registered');

		
            redirect('admin/manageitems');
            
        }
  
    }

    // public function delete_product_picture($imageid){
    //     if(!$this->session->admindata('restaurant_admin_id'))
	// 	{
	// 		redirect('admin/login');
	// 	}
		
    //     $this->db->where('productimgid', $imageid);
    //     $this->db->delete('product_img');
        
    //     $item_id = $_GET['ProductID'];
    //     redirect('admin/edit_product/'.$item_id);
    // }


    public function users(){
        // if(!$this->session->admindata('restaurant_admin_id'))
		// {
		// 	redirect('admin/login');
		// }
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/users.php');
        $this->load->view('templates/admin/footer.php');

    }

    public function add_user(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }
        $this->form_validation->set_rules('email', 'email', 'required');
        if($this->form_validation->run() === FALSE){
            $data['users'] = $this->admin_model->get_users();
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/add_user.php', $data);
        $this->load->view('templates/admin/footer.php');
        }
        else{
            $enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $this->admin_model->add_user($enc_password);
            redirect('admin/add_user');
        }
    }

    public function client(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }
        $this->form_validation->set_rules('email', 'email', 'required');
        if($this->form_validation->run() === FALSE){
            $data['users'] = $this->admin_model->get_users();
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/client.php', $data);
        $this->load->view('templates/admin/footer.php');
        }
        else{
            $enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $this->admin_model->add_user($enc_password);
            redirect('admin/client');
        }
    }
    

    public function ajax_edit_usermodal($userid){

        $data['users'] = $this->admin_model->get_userinfo($userid);
        $this->load->view('templates/ajax/edit_user.php', $data);
    }

    public function update_user(){
        
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
		}
		
        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('email', 'email', 'required');
		
        

        if($this->form_validation->run() === FALSE){

			$this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/client?fail');
        }
        else{

            $this->admin_model->update_user();
			
			$this->session->set_flashdata('user_registered', 'You are sucessfully registered');

		
            redirect('admin/client');
            
        }
  
    }

    public function blog(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }
        $data['blogs'] = $this->blog_model->get_blog();
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/blog.php',$data);
        $this->load->view('templates/admin/footer.php');

    }

    public function del_blog($blog_id){

        $this->blog_model->del_blog($blog_id);
        redirect('admin/blog');
        
    }  

    
	public function ajax_view_product($item_id){


		$data['item'] = $this->admin_model->get_single_items($item_id);
		
    // echo '<pre>';
	// 	print_r($data);
	// 	echo '<pre>';
	// 	die;
        $this->load->view('templates/ajax/viewproduct.php', $data);
    }
    

    public function cart()
	{
		// if(!$this->session->userdata('restaurant_admin_id'))
		// {
		// 	redirect('admin/login');
		// }
		$userid= $this->session->userdata('restaurant_admin_id');
		$currUser = $this->admin_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";
		
		

		$this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php', $data);
        $this->load->view('templates/admin/aside.php', $data);
        $this->load->view('templates/cart/index.php', $data);
		$this->load->view('templates/admin/footer.php');
		
		// redirect('cart/');
    }

    public function cat_menu(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }
        $this->form_validation->set_rules('name', 'name', 'required');
        if($this->form_validation->run() === FALSE){
        $data['menus'] = $this->admin_model->get_cat_menu();
       
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/cat_menu.php');
        $this->load->view('templates/admin/footer.php');
        }
        else{
            $imgname = $this->do_upload();

            $this->admin_model->cat_menu($imgname);
            redirect('admin/cat_menu');
        }
    }

    public function manage_cat_menu(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }
        $this->form_validation->set_rules('name', 'name', 'required');
        if($this->form_validation->run() === FALSE){
        $data['menus'] = $this->admin_model->get_cat_menu();
       
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/manage_cat_menu.php', $data);
        $this->load->view('templates/admin/footer.php');
        }
        else{
            $imgname = $this->do_upload();
            $this->admin_model->update_cat_menu($imgname);
            redirect('admin/manage_cat_menu');
        }
    }

    public function del_cat_menu($cat_menu_id){

        $this->admin_model->del_cat_menu($cat_menu_id);
        redirect('admin/manage_cat_menu');
        
    }

    public function ajax_edit_cat_menumodal($cat_menu_id){

        $data['menus'] = $this->admin_model->get_cat_menuinfo($cat_menu_id);
        $this->load->view('templates/ajax/edit_cat_menu.php', $data);
    }

    public function update_cat_menu(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
		}
		
        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('name', 'name', 'required');
		


        if($this->form_validation->run() === FALSE){
       

			$this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/Menu?fail');
        }
        else{
            $imgname = $this->do_upload();
            $this->admin_model->update_cat_menu($imgname); 
			
			$this->session->set_flashdata('user_registered', 'You are sucessfully registered');

		
            redirect('admin/manage_cat_menu');
            
        }
  
    }

    public function addproduct(){ 
		$data = array(
			'id'      => $this->input->post('productid'),
			'qty'     =>  $this->input->post('productqty'),
			'price'   => $this->input->post('productprice'),
			'name'    =>  $this->input->post('productname'),
			// 'options' => array('Size' => 'L', 'Color' => 'Red')
		);
		
		$this->cart->insert($data);
		redirect('admin/addorder');
		
	}

	public function deleterowcart($rowid){
		$this->cart->remove($rowid);
		redirect('admin/addorder');
	}


    public function viewcart(){

		$this->load->view('templates/admin/viewcart.php');
		
	}

    public function addorder(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }
        
        $data['items'] = $this->admin_model->get_items();
        
        if($this->form_validation->run() === FALSE){

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/addorder.php',$data);
        $this->load->view('templates/admin/footer.php');
    }
	else{


		$orderid = $this->admin_model->placeorder();
		foreach ($this->cart->contents() as $items):
		
			$this->adorderitem($items['price'], $items['qty'], $orderid, $items['id']);

		endforeach;
		$this->cart->destroy();
		redirect('admin/addorder');
	}
    }

    public function adorderitem($orderprice, $orderqty, $orderid, $item_id){

        $data = array(
            'sub_total' => $orderprice,
            'qty' => $orderqty,
            'order_id' => $orderid,
            'product_id' => $item_id
        );
        
        
        $this->security->xss_clean($data);
        $this->db->insert('order_item', $data);


}

public function billing()
	{
		// if(!$this->session->admindata('restaurant_admin_id'))
		// {
		// 	redirect('users/login');
		// }
		
		$data['title'] = "User Account";
		$this->form_validation->set_rules('fullname', 'fullname', 'required');
       if($this->form_validation->run() === FALSE){

		$this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php', $data);
        $this->load->view('templates/admin/aside.php', $data);
        $this->load->view('templates/admin/billing.php', $data);
        $this->load->view('templates/admin/footer.php');
	}
	else{


		$orderid = $this->admin_model->placeorder();
		foreach ($this->cart->contents() as $items):
		
			$this->adorderitem($items['price'], $items['qty'], $orderid, $items['id']);

		endforeach;
		$this->cart->destroy();
		redirect('admin/addorder');
	}
	}


    public function manageorder(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }
        
        $data['orders'] = $this->admin_model->get_placeorder();
       
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/manageorder.php', $data);
        $this->load->view('templates/admin/footer.php');
      
    }

    public function del_placeorder($id){
		$this->admin_model->del_placeorder($id);
		redirect('admin/manageorder');
	}

    public function deliveredorder(){
        // if(!$this->session->admindata('restaurant_admin_id'))
		// {
		// 	redirect('admin/login');
		// }
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/deliveredorder.php');
        $this->load->view('templates/admin/footer.php');

    }

    public function cancleorder(){
        // if(!$this->session->admindata('restaurant_admin_id'))
		// {
		// 	redirect('admin/login');
		// }
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/cancleorder.php');
        $this->load->view('templates/admin/footer.php');

    }

    public function add_options(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }
        
        $this->form_validation->set_rules('OptionName', 'OptionName', 'required');
       if($this->form_validation->run() === FALSE){
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/add_options.php');
        $this->load->view('templates/admin/footer.php');
       }
       else{
            $this->admin_model->add_options();
            redirect('admin/add_options');
       }
    }

    public function manageoptions(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }
        
        $this->form_validation->set_rules('OptionName', 'OptionName', 'required');
       if($this->form_validation->run() === FALSE){
        $data['options'] = $this->admin_model->get_options();
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/manageoptions.php', $data);
        $this->load->view('templates/admin/footer.php');
    }
    else{
         $this->admin_model->update_options();
         redirect('admin/manageoptions');
    }
    }

    public function del_options($options_id){

        $this->admin_model->del_options($options_id);
        redirect('admin/manageoptions');
        
    }

    public function ajax_edit_optionmodal($options_id){

        $data['options'] = $this->admin_model->get_optionsinfo($options_id);
        $this->load->view('templates/ajax/edit_options.php', $data);
    }

    public function update_options(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
		}
		
        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('OptionName', 'OptionName', 'required');
		


        if($this->form_validation->run() === FALSE){
       

			$this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/options?fail');
        }
        else{

            $this->admin_model->update_options(); 
			
			$this->session->set_flashdata('user_registered', 'You are sucessfully registered');

		
            redirect('admin/manageoptions');
            
        }
  
    }

    public function add_addons(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }
        
        $this->form_validation->set_rules('add_ons_name', 'add_ons_name', 'required');
       if($this->form_validation->run() === FALSE){
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/add_addons.php');
        $this->load->view('templates/admin/footer.php');
       }
       else{
            $imgname = $this->do_upload();

            $this->admin_model->add_addons($imgname);
            redirect('admin/add_addons');
       }
    }

    public function manage_addons(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }
        
        $this->form_validation->set_rules('add_ons_name', 'add_ons_name', 'required');
       if($this->form_validation->run() === FALSE){
        $data['addons'] = $this->admin_model->get_addons();
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/manage_addons.php', $data);
        $this->load->view('templates/admin/footer.php');
    }
    else{
         $imgname = $this->do_upload();

         $this->admin_model->update_addons($imgname);
         redirect('admin/manage_addons');
    }
    }

    public function del_addons($addons_id){

        $this->admin_model->del_addons($addons_id);
        redirect('admin/manage_addons');
        
    }

    public function ajax_edit_addonsmodal($addons_id){

        $data['addons'] = $this->admin_model->get_addonsinfo($addons_id);
        $this->load->view('templates/ajax/edit_addons.php', $data);
    }

    public function update_addons(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
		}
		
        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('add_ons_name', 'add_ons_name', 'required');
		


        if($this->form_validation->run() === FALSE){
       

			$this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/addons?fail');
        }
        else{
            $imgname = $this->do_upload();
            $this->admin_model->update_addons($imgname); 
			
			$this->session->set_flashdata('user_registered', 'You are sucessfully registered');

		
            redirect('admin/manage_addons');
            
        }
  
    }

    public function managecustomer(){
        // if(!$this->session->admindata('restaurant_admin_id'))
		// {
		// 	redirect('admin/login');
		// }
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/managecustomer.php');
        $this->load->view('templates/admin/footer.php');

    }

    public function recurring(){
        // if(!$this->session->admindata('restaurant_admin_id'))
		// {
		// 	redirect('admin/login');
		// }
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/recurring.php');
        $this->load->view('templates/admin/footer.php');

    }

    public function return(){
        // if(!$this->session->admindata('restaurant_admin_id'))
		// {
		// 	redirect('admin/login');
		// }
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/return.php');
        $this->load->view('templates/admin/footer.php');

    }

    public function setting(){
        // if(!$this->session->admindata('restaurant_admin_id'))
		// {
		// 	redirect('admin/login');
		// }
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/setting.php');
        $this->load->view('templates/admin/footer.php');

    }

    public function totalsales(){
        // if(!$this->session->admindata('restaurant_admin_id'))
		// {
		// 	redirect('admin/login');
		// }
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/totalsales.php');
        $this->load->view('templates/admin/footer.php');

    }

    public function seller(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }

        $this->form_validation->set_rules('name', 'name', 'required');
        if($this->form_validation->run() === FALSE){
        
            $data['sellers'] = $this->admin_model->get_seller();
        
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/seller.php',$data);
        $this->load->view('templates/admin/footer.php');
        }
        else{


            $enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            
            $this->admin_model->add_seller($enc_password);
            redirect('admin/seller');
        }
    }
    
   
    public function ajax_edit_sellermodal($sellerid){

        $data['sellers'] = $this->admin_model->get_sellerinfo($sellerid);
        $this->load->view('templates/ajax/edit_seller.php', $data);
    }

    public function update_seller(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
		}
		
        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('name', 'name', 'required');
		
        

        if($this->form_validation->run() === FALSE){

			$this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/seller?fail');
        }
        else{

            $this->admin_model->update_seller();
			
			$this->session->set_flashdata('user_registered', 'You are sucessfully registered');

		
            redirect('admin/seller');
            
        }
  
    }


    public function add_staff(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }
        
        $this->form_validation->set_rules('name', 'name', 'required');
        if($this->form_validation->run() === FALSE){
        
            $data['Staffs'] = $this->admin_model->get_staff();
            $data['roles'] = $this->admin_model->get_role();
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/add_staff.php', $data);
        $this->load->view('templates/admin/footer.php');
    }
    else{
        $enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);

        $this->admin_model->add_staff($enc_password);
        redirect('admin/add_staff');
    }
    }


    public function administrator(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }
        
        $this->form_validation->set_rules('name', 'name', 'required');
        if($this->form_validation->run() === FALSE){
        
            $data['Staffs'] = $this->admin_model->get_staff();
            $data['roles'] = $this->admin_model->get_role();
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/administrator.php', $data);
        $this->load->view('templates/admin/footer.php');
    }
    else{
        $enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);

        $this->admin_model->add_staff($enc_password);
        redirect('admin/administrator');
    }
    }

    
    
    public function ajax_edit_staffmodal($staffid){

        $data['Staffs'] = $this->admin_model->get_staffinfo($staffid);
        $data['roles'] = $this->admin_model->get_role();
        $this->load->view('templates/ajax/editstaff.php', $data);
    }

    public function updatestaff(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
		}
		
        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('name', 'name', 'required');
		
        

        if($this->form_validation->run() === FALSE){
            $data['roles'] = $this->admin_model->get_role();

			$this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/administrator?fail');
        }
        else{
           
            $this->admin_model->updatestaff();
			
			$this->session->set_flashdata('user_registered', 'You are sucessfully registered');

		
            redirect('admin/administrator');
            
        }
  
    }


    public function role(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
        }
        
        $this->form_validation->set_rules('role', 'role', 'required');
        if($this->form_validation->run() === FALSE){
        
            $data['roles'] = $this->admin_model->get_role();
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/role.php', $data);
        $this->load->view('templates/admin/footer.php');
        }
        else{
            $this->admin_model->add_role();
            redirect('admin/role');
        }
    }
    
    public function ajax_edit_rolemodal($roleid){

        $data['roles'] = $this->admin_model->get_roleinfo($roleid);
        $this->load->view('templates/ajax/editrole.php', $data);
    }

    public function update_role(){
        if(!$this->session->admindata('restaurant_admin_id'))
		{
			redirect('admin/login');
		}
		
        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('role', 'role', 'required');
		
        

        if($this->form_validation->run() === FALSE){
           

			$this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/role?fail');
        }
        else{
           
            $this->admin_model->update_role();
			
			$this->session->set_flashdata('user_registered', 'You are sucessfully registered');

		
            redirect('admin/role');
            
        }
  
    }

    public function country(){
        // if(!$this->session->admindata('restaurant_admin_id'))
		// {
		// 	redirect('admin/login');
		// }
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/country.php');
        $this->load->view('templates/admin/footer.php');

    }
    function do_upload() {
		
		$config = array(
			'upload_path' => "assets/uploads/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => false,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "5000",
			'max_width' => "5000"
			);
	
		$this->load->library('upload', $config);

		if($this->upload->do_upload('userfile'))
		{
			$imgdata = array('upload_data' => $this->upload->data());
	
			$imgname = $imgdata['upload_data']['file_name'];
		}
		else
		{
			$error = array('error' => $this->upload->display_errors());
			echo '<pre>';
			print_r($error);
			echo '<pre>';
			exit;
		}

        return $imgname;
    }

    public function uploadImage() { 
   
        $data = [];
  
        $count = count($_FILES['files']['name']);
  
      
  
        for($i=0;$i<$count;$i++){
  
      
  
          if(!empty($_FILES['files']['name'][$i])){
  
      
  
            $_FILES['file']['name'] = $_FILES['files']['name'][$i];
  
            $_FILES['file']['type'] = $_FILES['files']['type'][$i];
  
            $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
  
            $_FILES['file']['error'] = $_FILES['files']['error'][$i];
  
            $_FILES['file']['size'] = $_FILES['files']['size'][$i];
  
    
  
            $config['upload_path'] = 'assets/uploads/'; 
  
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
  
            $config['max_size'] = '5000';
  
            $config['file_name'] = $_FILES['files']['name'][$i];
  
     
  
            $this->load->library('upload',$config); 
  
      
  
            if($this->upload->do_upload('file')){
  
              $uploadData = $this->upload->data();
  
              $filename = $uploadData['file_name'];
  
     
  
              $data['totalFiles'][] = $filename;
  
            }
  
          }
  
        }
        $item_id = $this->input->post('productid');
     
        foreach($data['totalFiles'] as $carimage){
            $this->admin_model->add_image($item_id, $carimage);
        }
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        
        redirect('admin/edit_product/'.$item_id);
        // $this->load->view('imageUploadForm', $data); 
  
     }

//deals start

public function add_deal(){
    if(!$this->session->admindata('restaurant_admin_id'))
    {
        redirect('admin/login');
    }
    $this->form_validation->set_rules('deal_name', 'deal_name', 'required');
    if($this->form_validation->run() === FALSE){
    $data['deals'] = $this->admin_model->get_deal();
   
    $this->load->view('templates/admin/header.php');
    $this->load->view('templates/admin/navbar.php');
    $this->load->view('templates/admin/aside.php');
    $this->load->view('templates/admin/add_deal.php');
    $this->load->view('templates/admin/footer.php');
    }
    else{
        
        $imgname = $this->do_upload();
        $this->admin_model->add_deal($imgname);
        redirect('admin/add_deal');
    }
}

public function managedeal(){
    if(!$this->session->admindata('restaurant_admin_id'))
    {
        redirect('admin/login');
    }
    $this->form_validation->set_rules('deal_name', 'deal_name', 'required');
    if($this->form_validation->run() === FALSE){
    $data['deals'] = $this->admin_model->get_deal();
   
    $this->load->view('templates/admin/header.php');
    $this->load->view('templates/admin/navbar.php');
    $this->load->view('templates/admin/aside.php');
    $this->load->view('templates/admin/managedeal.php', $data);
    $this->load->view('templates/admin/footer.php');
    }
    else{
        $imgname = $this->do_upload();
        $this->admin_model->update_deal($imgname);
        redirect('admin/managedeal');
    }
}

public function del_deal($deal_id){

    $this->admin_model->del_deal($deal_id);
    redirect('admin/managedeal');
    
}

public function ajax_edit_dealmodal($deal_id){

    $data['deals'] = $this->admin_model->get_dealinfo($deal_id);
    $this->load->view('templates/ajax/edit_deal.php', $data);
}

public function update_deal(){
    if(!$this->session->admindata('restaurant_admin_id'))
    {
        redirect('admin/login');
    }
    
    $data['title'] = 'Sign Up';
    $this->form_validation->set_rules('deal_name', 'deal_name', 'required');
    


    if($this->form_validation->run() === FALSE){
   

        $this->session->set_flashdata('user_error', 'something missing');
        redirect('admin/deal?fail');
    }
    else{
        $imgname = $this->do_upload();
        $this->admin_model->update_deal($imgname); 
        
        $this->session->set_flashdata('user_registered', 'You are sucessfully registered');

    
        redirect('admin/managedeal');
        
    }

}






//deals end



// start All Fields api

    //  public function get_all_product(){
    //     $data['data'] = $this->admin_model->get_product_api();
    //     echo json_encode($data);
    // }

    public function get_product_api()
    { $item_id = $this->input->get('productid');
        $data['data'] = $this->admin_model->get_product_api($item_id);
        echo json_encode($data);
    }

    public function get_edit_product_api(){
        $data['data'] = $this->admin_model->get_productinfo_api();
        echo json_encode($data);
    }

    public function get_cat_menu_api(){
        $data['data'] = $this->admin_model->get_cat_menu_api();
        echo json_encode($data);
    }

    public function get_deal_api(){
        $data['data'] = $this->admin_model->get_deal_api();
        echo json_encode($data);
    }

    public function get_categories_api(){
        $data['data'] = $this->admin_model->get_cat_api();
        echo json_encode($data);
    }
    
    public function get_edit_categories_api(){
        $data['data'] = $this->admin_model->get_categoryinfo_api();
        echo json_encode($data);
    }
    
    public function get_users_api(){
        $data['data'] = $this->admin_model->get_users_api();
        echo json_encode($data);
    }

    public function get_edit_users_api(){
        $data['data'] = $this->admin_model->get_userinfo_api();
        echo json_encode($data);
    }
    
    public function get_staff_api(){
        $data['data'] = $this->admin_model->get_staff_api();
        echo json_encode($data);
    }
    
    public function get_all_edit_staff(){
        $data['data'] = $this->admin_model->get_staffinfo_api();
        echo json_encode($data);
    }
    
    public function get_order_api(){
        $data['data'] = $this->admin_model->get_placeorder_api();
        echo json_encode($data);
    }
    
    public function get_login_api()
    {
        $username = $this->input->get('username');
        $password = $this->input->get('password');
        echo  json_encode($this->admin_model->mlogin($username, $password));
    }

// End All Fields api 
}

