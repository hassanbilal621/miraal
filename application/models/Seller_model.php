<?php
class Seller_model extends CI_Model{

	   // Product Start Get / Add / Update / Delete 
	   
	   public function login($username, $password){
	

		$this->db->where('email', $username);
		$result = $this->db->get('seller');

	
        if($result->num_rows() == 1){
			$hash = $result->row(0)->password;
			
			if (password_verify($password, $hash))
			{
				return $result->row(0)->seller_id;
			}
			else
			{
				return false;
			}
            
			
			
        } else {
            return false;
        }
	}
	
	public function register($enc_password){ 
        $data = array(
            'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'password' => $enc_password,
			'date' => date('Y-m-d H:i:s')
        );

		$this->security->xss_clean($data);
		return $this->db->insert('seller', $data);

	}

	public function add_product()
	{
		$data = array(
            'ProductSKU' => $this->input->post('productsku'),
			'ProductName' => $this->input->post('productname'),
			'ProductPrice' => $this->input->post('productprice'),
			'ProductWeight' => $this->input->post('productweight'),
			'ProductCartDesc' => $this->input->post('productcartdesc'),
			'ProductShortDesc' => $this->input->post('productshortdesc'),
			'ProductLongDesc' => $this->input->post('productlongdesc'),
			'ProductThumb' => $this->input->post('productthumb'),
			//'country' => $this->input->post('country'),
            //'city' => $this->input->post('city'),
			'ProductCategoryID' => $this->input->post('categoryid'),
			'ProductUpdateDate' =>date('Y-m-d H:i:s'),
			'ProductStock' => $this->input->post('productstock'),
			'ProductLive' => $this->input->post('productlive'),
			'ProductUnlimited' => $this->input->post('productunlimited'),
			'ProductLocation' => $this->input->post('productlocation'),
			'product_brand_id' => $this->input->post('productbrandid'),
			'productimg_id' => $this->input->post('productimg_id')
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('products', $data);
	}

	public function update_product($productid)
	{
		$data = array(
            'ProductSKU' => $this->input->post('productsku'),
			'ProductName' => $this->input->post('productname'),
			'ProductPrice' => $this->input->post('productprice'),
			'ProductWeight' => $this->input->post('productweight'),
			'ProductCartDesc' => $this->input->post('productcartdesc'),
			'ProductShortDesc' => $this->input->post('productshortdesc'),
			'ProductLongDesc' => $this->input->post('productlongdesc'),
			'ProductThumb' => $this->input->post('productthumb'),
			//'country' => $this->input->post('country'),
            //'city' => $this->input->post('city'),
			'ProductCategoryID' => $this->input->post('categoryid'),
			'ProductUpdateDate' =>date('Y-m-d H:i:s'),
			'ProductStock' => $this->input->post('productstock'),
			'ProductLive' => $this->input->post('productlive'),
			'ProductUnlimited' => $this->input->post('productunlimited'),
			'ProductLocation' => $this->input->post('productlocation'),
			'product_brand_id' => $this->input->post('productbrandid'),
			'productimg_id' => $this->input->post('productimg_id')
        );
		
		$this->security->xss_clean($data);
		$this->db->where('ProductID', $productid);
		$this->db->update('products', $data);

	}

	public function get_product(){
	
		$this->db->join('productcategories','productcategories.CategoryID = products.ProductCategoryID','left');
		$this->db->join('brands','brands.brand_id = products.product_brand_id','left');
		$this->db->join('product_img','product_img.productimg_id = products.productimg_id','left');

        $query = $this->db->get('products');
        return $query->result_array();
	}

	public function get_editproduct($productid){
	
		$this->db->join('productcategories','productcategories.CategoryID = products.ProductCategoryID','left');
		$this->db->join('brands','brands.brand_id = products.product_brand_id','left');
		$this->db->where('ProductID', $productid);
        $query = $this->db->get('products');
        return $query->row_array();
	}


	public function del_product($productid)
	{
		$this->db->where('ProductID', $productid);
		$this->db->delete('products');
	}

	public function add_image($productid, $carimage)
    { 
        
        $data = array( 
            
            'product_img' => $carimage,
			'product_id' => $productid
			
        );
        $this->security->xss_clean($data);
        $this->db->insert('product_img', $data);
	}

	


	public function get_product_image($productid){

		$this->db->where('product_id', $productid);

		$query = $this->db->get('product_img');
		
        return $query->result_array();
	}

	
	public function get_image(){
	
		$this->db->join('products','products.ProductID = product_img.product_id','left');
        $query = $this->db->get('product_img');
        return $query->result_array();
	}

// Product End 
}