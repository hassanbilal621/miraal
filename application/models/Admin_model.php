<?php
class Admin_model extends CI_Model{
	public function __construct()
    {
      parent::__construct();
	  $this->load->library('cart');
	}
	
	public function mlogin($username, $password)
    {
        $this->db->where('username', $username);
        $result = $this->db->get('admin');
 
        if ($result->num_rows() == 1) {
            $hash = $result->row(0)->password;
 
            if (password_verify($password, $hash)) {
                return $result->row_array();
            } else { 
                $data=array('error'=>'wrong_password');
                return $data;
            }
        } else {
            $data=array('error'=>'no_account');
            return $data;
        }
    }
    
    public function login($username, $password){

        $this->db->where('username', $username);
        $result = $this->db->get('admin');

        if($result->num_rows() == 1){
			$hash = $result->row(0)->password;
			
			if (password_verify($password, $hash))
			{
				return $result->row(0)->id;
			}
			else
			{
				return false;
			}
			
			
        } else {
            return false;
        }
	}


		
	public function get_admininfo($admin_id)
	{
		$this->db->where('id', $admin_id);
		$result = $this->db->get('admin');
		
		return $result->row_array();
	}

	public function get_bloggerinfo($blogger_id)
	{
		$this->db->where('id', $blogger_id);
		$result = $this->db->get('blogger');
		
		return $result->row_array();
	}



	// Items Start Get / Add / Update / Delete 

	public function add_items($imgname)
	{
		$data = array(
            'item_name' => $this->input->post('item_name'),
			'item_price' => $this->input->post('item_price'),
			'item_image' => $imgname,
			'item_type_id' => $this->input->post('type_id'),
			'item_menu_id' => $this->input->post('menu_id')
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('items', $data);
	}

	public function update_items($imgname)
	{
		$data = array(
            'item_name' => $this->input->post('item_name'),
			'item_price' => $this->input->post('item_price'),
			'item_image' => $imgname,
			'item_type_id' => $this->input->post('type_id'),
			'item_menu_id' => $this->input->post('menu_id')
        );
		
		$this->security->xss_clean($data);
		$this->db->where('item_id', $this->input->post('item_id'));
		$this->db->update('items', $data);

	}

	public function get_single_items($item_id)
    {

		$this->db->where('item_id', $item_id);
		$this->db->join('items_type','items_type.item_type_id = items.item_type_id','left');
		$this->db->join('menu','menu.cat_menu_id = items.item_menu_id','left');

		$result = $this->db->get('items');
		
		return $result->row_array();
	}

	public function get_items(){
	
		$this->db->join('items_type','items_type.item_type_id = items.item_type_id','left');
		$this->db->join('menu','menu.cat_menu_id = items.item_menu_id','left');

        $query = $this->db->get('items');
        return $query->result_array();
	}

	

	public function get_items_api($item_id){
	
		$this->db->join('items_type','items_type.item_type_id = items.item_type_id','left');
		$this->db->join('menu','menu.cat_menu_id = items.item_menu_id','left');
        $query = $this->db->get('items', $item_id);
        return $query->result_array();
	}
	


	public function get_edititems($item_id){
	
		$this->db->join('items_type','items_type.item_type_id = items.item_type_id','left');
		$this->db->where('item_id', $item_id);
        $query = $this->db->get('items');
        return $query->row_array();
	}


	public function del_items($item_id)
	{
		$this->db->where('item_id', $item_id);
		$this->db->delete('items');
	}

	

	public function get_itemsinfo($item_id)
	{
		$this->db->where('item_id', $item_id);
		$result = $this->db->get('items');
		
		return $result->row_array();
	}

	public function get_itemsinfo_api($item_id)
	{
		$this->db->where('item_id', $item_id);
		$result = $this->db->get('items');
		
		return $result->row_array();
	}

// Items End 


// Category Start Get / Add / Update / Delete 

 public function get_itemtypeinfo($type_id)
	{
		$this->db->where('item_type_id', $type_id);
		$result = $this->db->get('items_type');
		
		return $result->row_array();
	}

	public function get_itemtypeinfo_api($type_id)
	{
		$this->db->where('item_type_id', $type_id);
		$result = $this->db->get('items_type');
		
		return $result->row_array();
	}

public function add_itemtype()
	{
		$data = array(
            
			'item_type_name' => $this->input->post('item_type_name')
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('items_type', $data);
	}

	
public function update_itemtype() 
{
	$data = array(
		
		'item_type_name' => $this->input->post('item_type_name')
		
	);
	
	$this->security->xss_clean($data);
	$this->db->where('item_type_id', $this->input->post('item_type_id'));
	$this->db->update('items_type', $data);
}

	public function get_itemtype(){

		
	
        $query = $this->db->get('items_type');
        return $query->result_array();
	}

	public function get_itemtype_api(){

		$query = $this->db->get('items_type');
        return $query->result_array();
	}

	public function del_itemtype($type_id)
	{
		$this->db->where('item_type_id', $type_id);
		$this->db->delete('items_type');
	}

// Category End 

// place order start //
public function placeorder()
	{
		$data = array(

            // 'DetailOrderID' => $this->input->post('order_id'),
            // 'Detailitem_id' =>$item_id,
			'fullname' => $this->input->post('fullname'),
			'phone' => $this->input->post('phone'),
			'email' => $this->input->post('email'),
			'address' => $this->input->post('address'),
			'city' => $this->input->post('city'),
			'payment' => $this->input->post('payment'),
			'comment' => $this->input->post('comment'),
			'totalamount' => $this->cart->total()
        );
		
		
		$this->security->xss_clean($data);
		$this->db->insert('orders', $data);

		return $this->db->insert_id();

	}

	public function get_placeorder(){ 


		$this->db->join('orders', 'orders.orderid = order_item.order_id', 'left');
		$this->db->join('items', 'items.item_id = order_item.product_id', 'left');
        $query = $this->db->get('order_item');
		return $query->result_array();
		
	}

	public function get_placeorder_api(){ 

		$this->db->join('items', 'items.item_id = order_item.product_id', 'left');
        $query = $this->db->get('order_item');
        return $query->result_array();
	}


	public function del_placeorder($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('order_item');
	}


// Role Start Get / Add / Update / Delete 

public function add_role()
	{
		$data = array(
            'role' => $this->input->post('role')
			
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('role', $data);
	}

	public function update_role()
	{
		$data = array(
            'role' => $this->input->post('role')
			
        );
		
		$this->security->xss_clean($data);
		$this->db->where('role_id', $this->input->post('role_id'));
        $this->db->update('role', $data);
	}

	public function get_roleinfo($roleid)
	{
		$this->db->where('role_id', $roleid);
		$result = $this->db->get('role');
		
		return $result->row_array();
	}

	public function get_role(){
        $query = $this->db->get('role');
        return $query->result_array();
	}

	public function del_role($roleid)
	{
		$this->db->where('role_id', $roleid);
		$this->db->delete('role');
	}

// Role End

// addons Start Get / Add / Update / Delete 

public function add_addons($imgname)
	{
		$data = array(
			'add_ons_name' => $this->input->post('add_ons_name'),
			'add_ons_price' => $this->input->post('add_ons_price'),
			'add_ons_img' => $imgname
			
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('addons', $data);
	}

	public function update_addons($imgname)
	{
		$data = array(
            'add_ons_name' => $this->input->post('add_ons_name'),
			'add_ons_price' => $this->input->post('add_ons_price'),
			'add_ons_img' => $imgname
			
        );
		
		$this->security->xss_clean($data);
		$this->db->where('add_ons_id', $this->input->post('add_ons_id'));
        $this->db->update('addons', $data);
	}

	public function get_addonsinfo($addons_id)
	{
		$this->db->where('add_ons_id', $addons_id);
		$result = $this->db->get('addons');
		
		return $result->row_array();
	}

	public function get_addons(){
        $query = $this->db->get('addons');
        return $query->result_array();
	}

	public function del_addons($addons_id)
	{
		$this->db->where('add_ons_id', $addons_id);
		$this->db->delete('addons');
	}

// addons End

// Options Start Get / Add / Update / Delete 

public function add_options()
	{
		$data = array(
			'OptionName' => $this->input->post('OptionName')
			
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('options', $data);
	}

	public function update_options()
	{
		$data = array(
            'OptionName' => $this->input->post('OptionName')
			
        );
		
		$this->security->xss_clean($data);
		$this->db->where('OptionID', $this->input->post('OptionID'));
        $this->db->update('options', $data);
	}

	public function get_optionsinfo($options_id)
	{
		$this->db->where('OptionID', $options_id);
		$result = $this->db->get('options');
		
		return $result->row_array();
	}

	public function get_options(){
        $query = $this->db->get('options');
        return $query->result_array();
	}

	public function del_options($options_id)
	{
		$this->db->where('OptionID', $options_id);
		$this->db->delete('options');
	}

// Options End


// category menu Start Get / Add / Update / Delete 

public function cat_menu($imgname)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'img' => $imgname
			
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('menu', $data);
	}

	public function update_cat_menu($imgname)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'img' => $imgname
			
        );
		
		$this->security->xss_clean($data);
		$this->db->where('cat_menu_id', $this->input->post('cat_menu_id'));
        $this->db->update('menu', $data);
	}

	public function get_cat_menuinfo($cat_menu_id)
	{
		$this->db->where('cat_menu_id', $cat_menu_id);
		$result = $this->db->get('menu');
		
		return $result->row_array();
	}

	public function get_cat_menu(){
        $query = $this->db->get('menu');
        return $query->result_array();
	}
	
	public function get_cat_menu_api(){
        $query = $this->db->get('menu');
        return $query->result_array();
	}

	public function del_cat_menu($cat_menu_id)
	{
		$this->db->where('cat_menu_id', $cat_menu_id);
		$this->db->delete('menu');
	}

// category menu End

// offers Start Get / Add / Update / Delete 

public function add_deal($imgname)
	{
		$data = array(
			'deal_name' => $this->input->post('deal_name'),
			'deal_price' => $this->input->post('deal_price'),
			'deal_description' => $this->input->post('deal_description'),
			'img' => $imgname
			
			
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('offers', $data);
	}

	public function update_deal($imgname)
	{
		$data = array(
			'deal_name' => $this->input->post('deal_name'),
			'deal_price' => $this->input->post('deal_price'),
			'deal_description' => $this->input->post('deal_description'),
			'img' => $imgname

			
			
        );
		
		$this->security->xss_clean($data);
		$this->db->where('deal_id', $this->input->post('deal_id'));
        $this->db->update('offers', $data);
	}

	public function get_dealinfo($deal_id)
	{
		$this->db->where('deal_id', $deal_id);
		$result = $this->db->get('offers');
		
		return $result->row_array();
	}

	public function get_deal(){
        $query = $this->db->get('offers');
        return $query->result_array();
	}
	
	public function get_deal_api(){
        $query = $this->db->get('offers');
        return $query->result_array();
	}

	public function del_deal($deal_id)
	{
		$this->db->where('deal_id', $deal_id);
		$this->db->delete('offers');
	}

// offers End

// Staff Start Get / Add / Update / Delete 

public function add_staff($enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'phone' => $this->input->post('phone'),
			'email' => $this->input->post('email'),
			'password' => $enc_password,
			'date' => date('Y-m-d H:i:s'),
            'role_id' => $this->input->post('roleid')
			
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('staff', $data);
	}

	public function updatestaff()
	{
		$data = array(
			'name' => $this->input->post('name'),
			'phone' => $this->input->post('phone'),
			'email' => $this->input->post('email'),
			'date' => date('Y-m-d H:i:s'),
            'role_id' => $this->input->post('roleid')
			
        );
		
		$this->security->xss_clean($data);
		$this->db->where('staff_id', $this->input->post('staff_id'));
        $this->db->update('staff', $data);
	}

	public function get_staff(){

		$this->db->join('role','role.role_id = staff.role_id','left');

        $query = $this->db->get('staff');
        return $query->result_array();
	}

	public function get_staff_api(){

		$this->db->join('role','role.role_id = staff.role_id','left');

        $query = $this->db->get('staff');
        return $query->result_array();
	}
	
	public function get_staffinfo($staffid)
	{
		$this->db->join('role','role.role_id = staff.role_id','left');

		$this->db->where('staff_id', $staffid);
		$result = $this->db->get('staff');
		
		return $result->row_array();
	}

	public function get_staffinfo_api($staffid)
	{
		$this->db->join('role','role.role_id = staff.role_id','left');

		$this->db->where('staff_id', $staffid);
		$result = $this->db->get('staff');
		
		return $result->row_array();
	}

	public function del_staff($staffid)
	{
		$this->db->where('staff_id', $staffid);
		$this->db->delete('staff');
	}


// staff End 

	// Users Get / Add / Update / Delete 
	
	public function add_user($enc_password)
	{
		$data = array(
            'username' => $this->input->post('username'),
			'email' => $this->input->post('email'),
			'password' => $enc_password,
			'UserRegistrationDate' => date('Y-m-d H:i:s')
			//'country' => $this->input->post('country'),
            //'city' => $this->input->post('city'),
			// 'picture' => $imgname
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('users', $data);
	}

	public function update_user()
	{
		$data = array(
			'username' => $this->input->post('username'),
			'email' => $this->input->post('email')
        );
		
		$this->security->xss_clean($data);
		$this->db->where('id', $this->input->post('id'));
		$this->db->update('users', $data);

	}
	
	public function get_userinfo($userid)
	{
		$this->db->where('id', $userid);
		$result = $this->db->get('users');
		
		return $result->row_array();
	}

	public function get_userinfo_api($userid)
	{
		$this->db->where('id', $userid);
		$result = $this->db->get('users');
		
		return $result->row_array();
	}

	public function del_user($userid)
	{
		$this->db->where('id', $userid);
		$this->db->delete('users');
	}

	public function get_users(){
        $this->db->order_by('users.id', 'DESC');
        $query = $this->db->get('users');
        return $query->result_array();
	}

	public function get_users_api(){
        $this->db->order_by('users.id', 'DESC');
        $query = $this->db->get('users');
        return $query->result_array();
	}

	public function get_orders(){
		$this->db->order_by('users_orders.orderid', 'DESC');
		$this->db->join('academic_level', 'users_orders.academic_level_id = academic_level.academic_level_id', 'left');
		$this->db->join('typesofpaper', 'users_orders.typesofpaper_id = typesofpaper.typeofpaper_id', 'left');
		$this->db->join('noofdays', 'users_orders.noofday_id = noofdays.noofdays_id', 'left');
		$this->db->join('noofpages', 'users_orders.noofpage_id = noofpages.noofpages_id', 'left');

        $query = $this->db->get('users_orders');
        return $query->result_array();
	}

	public function get_invoices(){
		$this->db->order_by('users_orders.orderid', 'DESC');
		$this->db->join('academic_level', 'users_orders.academic_level_id = academic_level.academic_level_id', 'left');
		$this->db->join('typesofpaper', 'users_orders.typesofpaper_id = typesofpaper.typeofpaper_id', 'left');
		$this->db->join('noofdays', 'users_orders.noofday_id = noofdays.noofdays_id', 'left');
		$this->db->join('noofpages', 'users_orders.noofpage_id = noofpages.noofpages_id', 'left');

        $query = $this->db->get('users_orders');
        return $query->result_array();
	}

	public function get_countries(){
        $this->db->order_by('countries.country_name', 'ASC');
        $query = $this->db->get('countries');
        return $query->result_array();
	}

	
	public function get_cities($country_name){
		$this->db->where('country_name', $country_name);
        $this->db->order_by('cities.city_name', 'ASC');
        $query = $this->db->get('cities');
        return $query->result_array();
	}

	public function get_bloggers(){
        $this->db->order_by('blogger.id', 'DESC');
        $query = $this->db->get('blogger');
        return $query->result_array();
	}

	public function get_admins(){
        $this->db->order_by('admin.id', 'DESC');
        $query = $this->db->get('admin');
        return $query->result_array();
    }

	public function add_admin($enc_password)
	{
		$data = array(
            'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
			'password' => $enc_password,
			'register_date' => date('Y-m-d H:i:s')
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('admin', $data);
	}

	public function add_city()
	{
		$data = array(
            'geoname_id' => $this->input->post('geoname_id'),
			'country_iso_code' => $this->input->post('country_iso_code'),
			'country_name' => $this->input->post('country_name'),
			'time_zone' => $this->input->post('time_zone'),
			'city_name' => $this->input->post('city_name')
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('cities', $data);
	}

	
	
	// public function add_blogger($enc_password)
	// {
	// 	$data = array(
    //         'name' => $this->input->post('name'),
	// 		'username' => $this->input->post('username'),
	// 		'password' => $enc_password,
	// 		'register_date' => date('Y-m-d H:i:s')
    //     );
		
	// 	$this->security->xss_clean($data);
    //     $this->db->insert('blogger', $data);
	// }

	public function update_admin($admin_id, $enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
            'password' => $enc_password
        );
		
		$this->security->xss_clean($data);
		$this->db->where('id', $admin_id);
		$this->db->update('admin', $data);

	}


	public function update_blogger($blogger_id, $enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
            'password' => $enc_password
		);
		$this->security->xss_clean($data);
		$this->db->where('id', $blogger_id);
		$this->db->update('blogger', $data);
	}
	

	public function del_admin($adminid)
	{
		$this->db->where('id', $adminid);
		$this->db->delete('admin');
	}
	public function del_bloggers($bloggerid)
	{
		$this->db->where('id', $bloggerid);
		$this->db->delete('blogger');
	}
	public function del_city($city_name)
	{
		$this->db->where('city_name', $city_name);
		$this->db->delete('cities');
	}

	public function get_jobs(){
		$this->db->order_by('jobs.jobid', 'DESC');
		$this->db->join('company', 'jobs.companyid = company.id', 'left');
		$query = $this->db->get('jobs');
		
        return $query->result_array();
	}

	public function get_approvejobs(){
		$this->db->order_by('jobs.jobid', 'DESC');
		$this->db->join('company', 'jobs.companyid = company.id', 'left');
		$query = $this->db->get('jobs');
		
        return $query->result_array();
	}
	
	public function get_pendingjobs(){
		$this->db->order_by('jobs.jobid', 'DESC');
		$this->db->join('company', 'jobs.companyid = company.id', 'left');
		$query = $this->db->get('jobs');
		
        return $query->result_array();
	}
	
	public function get_appliedjobs(){
		$this->db->order_by('applications.appid', 'DESC');
		$this->db->join('users', 'applications.userid = users.id', 'left');
		$this->db->join('jobs', 'applications.jobid = jobs.jobid', 'left');
		$query = $this->db->get('applications');
		
        return $query->result_array();
	}


	function do_upload() {
		
		$config = array(
			'upload_path' => "assets/uploads/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => false,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "5000",
			'max_width' => "5000"
			);
	
		$this->load->library('upload', $config);

		if($this->upload->do_upload('userfile'))
		{
			$imgdata = array('upload_data' => $this->upload->data());
	
			$imgname = $imgdata['upload_data']['file_name'];
		}
		else
		{
			$error = array('error' => $this->upload->display_errors());
			echo '<pre>';
			print_r($error);
			echo '<pre>';
			exit;
		}

        return $imgname;
    }


}