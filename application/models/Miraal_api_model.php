<?php
class Miraal_api_model extends CI_Model{
	public function __construct()
    {
      parent::__construct();
	  $this->load->database();
    }

	// public function get_academic_level(){
    //     $this->db->order_by('academic_level.academic_level_id', 'DESC');
	// 	$query = $this->db->get('academic_level');
				
    //     return $query->result_array();
    // }
    
    public function get_menus_api(){

        
        $query = $this->db->get('menu');
        return $query->result_array(); // for single row_array and for multiple result_array

    }


    public function get_items_api($catid){
        
        $this->db->where('item_menu_id', $catid);
        $this->db->where('item_status', 'enable');

        $query = $this->db->get('items');
   

        return $query->result_array(); // for single row_array and for multiple result_array

    }


    

    public function get_deal_api(){
        
        $this->db->where('deal_status', 'enable');

        $query = $this->db->get('deals');
   

        return $query->result_array(); // for single row_array and for multiple result_array

    }


	public function place_order_api(){

		$data = array(
            'totalamount' => $_POST['totalAmount'],
			'itemcount' => $_POST['itemCount'],
            'address' => $_POST['address'],
            'phone' => $_POST['phone'],
            'comment' => $_POST['comment'],
            'orderitems' => $_POST['orderitems'],
            'city' => $_POST['city'],
            'userid'=> $_POST['userid']
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('orders', $data);

	}

    public function get_all_orders(){

        //$this->db->where('item_menu_id', $catid);
        // $this->db->where('item_status', 'enable');

        $query = $this->db->get('orders');

        return $query->result_array();

    }


    public function get_user_orders($userid){

        $this->db->where('userid', $userid);
        $query = $this->db->get('orders');
        return $query->result_array();

    }



	public function register_user_api(){

        $enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);


		$data = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'password' => $enc_password,
			'address' => $this->input->post('address'),
			'number' => $this->input->post('number')
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('users', $data);

	}


    public function login($username, $password)
	{

		$this->db->where('email', $username);
        $result = $this->db->get('users');
        

        // echo $username. 'asdasd';
        // print_r($result);
        // die;

		if ($result->num_rows() == 1) {
			$hash = $result->row(0)->password;

			if (password_verify($password, $hash)) {
				return $result->row(0)->id;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

    public function get_user_detail($userid){
        $this->db->where('id', $userid);

        $result = $this->db->get('users');

        return $result->row_array();

    }


    public function get_order_details($orderid){
        $this->db->where('orderid', $orderid);

        $result = $this->db->get('orders');

        return $result->row_array();

    }

    

}