<?php

class Blog_model extends CI_Model{
    public function __construct(){
        $this->load->database();
    }
	/*
     * Function: get_jobs
     * Purpose: This method is responsible for retrieving jobs from the database and then returning them 
     * Params:  $jobnum= FALSE: this is to get a specific job
				$limit = 0: default value of 0, this is for the use of pagination
				$offset = "": default value of 0, this is for the use of pagination
				$where = "": this is the where clause for search results. 
							Should be in form: array('field'=>FIELD, 'value'=>VALUE)
     * Return: query result - single array if $jobnum is not FALSE
     */
    public function getblog(){
        $this->db->order_by('blog_posts.id', 'DESC');
        $query = $this->db->get('blog_posts', 5);
        return $query->result_array();
    }


    public function get_blogs($blogid)
    {

		$this->db->where('id', $blogid);
        $result = $this->db->get('blog_posts');
        
		
		return $result->row_array();
    }
    


    public function getcourses()
    {
        $this->db->order_by('courses.cid', 'DESC');
        $this->db->join('instructors', 'courses.course_cinstructor_id = instructors.instructor_id', 'left');
        $query = $this->db->get('courses', 6);
        return $query->result_array();
    }


    public function getallcourses()
    {
        $this->db->order_by('courses.cid', 'DESC');
        $this->db->join('instructors', 'courses.course_cinstructor_id = instructors.instructor_id', 'left');
        $query = $this->db->get('courses', 6);
        return $query->result_array();
    }

    public function getcoursecategories()
    {
        $this->db->order_by('course_category.course_category_id', 'DESC');
        $query = $this->db->get('course_category', 6);
        return $query->result_array();
    }

    

    public function single_course_category($categoryid)
    {
		$this->db->where('course_category_id', $categoryid);
        $result = $this->db->get('course_category');
        
		return $result->row_array();
    }
    
    
    public function getevents()
    {
        $this->db->order_by('events.event_id', 'DESC');
        $query = $this->db->get('events', 6);
        return $query->result_array();
    }
        
    
    public function gettestimonial()
    {
        $this->db->order_by('testimonial.testimonial_id', 'DESC');
        $query = $this->db->get('testimonial', 6);
        return $query->result_array();
    }
        
    
    public function getinstructors()
    {
        $this->db->order_by('instructors.instructor_id', 'DESC');
        $query = $this->db->get('instructors', 6);
        return $query->result_array();
    }



    
    

    public function getblogcategory()
    {
        $this->db->order_by('blogs_category.blog_cat_id', 'DESC');
        $query = $this->db->get('blogs_category');
        return $query->result_array();
	}

    public function get_blog_comments($blogid){
        $this->db->where('blogid', $blogid);
        $this->db->order_by('blog_comment.id', 'DESC');
        $query = $this->db->get('blog_comment', 10);
        return $query->result_array();
    }

// Start blog insert 

public function add_blog($imgname)
{
    $data = array(
        'blog_title' => $this->input->post('blog_title'),
        'blog_desc' => $this->input->post('blog_desc'),
        'blog_img' => $imgname
        
    );
    
    $this->security->xss_clean($data);
    $this->db->insert('blog', $data);
    
}

public function get_blog(){
	
	$query = $this->db->get('blog');
	return $query->result_array();
}

public function del_blog($blog_id)
	{
		$this->db->where('blog_id', $blog_id);
		$this->db->delete('blog');
	}

// End blog


}

?>